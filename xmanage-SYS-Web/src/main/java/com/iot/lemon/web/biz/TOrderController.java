package com.iot.lemon.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TOrder;
import com.iot.lemon.service.TOrderService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 订单  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-19
 */
@Controller
@RequestMapping("/order")
@Api(value = "订单接口", description = "订单接口")
public class TOrderController extends AppBaseController<TOrder, TOrderService> {
    @PutMapping("/read/list")
    @RequiresPermissions("order.read")
    @ApiOperation(value = "查询订单", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("orgaId", UserUtil.getMyOrgaId());
        return super.query(param);
    }
}
