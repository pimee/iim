package com.iot.lemon.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TCarPayment;
import com.iot.lemon.service.TCarPaymentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 车辆缴费记录  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/carPayment")
@Api(value = "车辆缴费记录接口", description = "车辆缴费记录接口")
public class TCarPaymentController extends BaseController<TCarPayment, TCarPaymentService> {
    @RequiresPermissions("car.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询车辆缴费记录", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("carPayment.read")
    @PutMapping(value = "/car/detail")
    @ApiOperation(value = "车辆缴费记录详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TCarPayment param = WebUtil.getParameter(request, TCarPayment.class);
        return super.get(param);
    }
}