package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TEquipment;
import com.iot.lemon.service.TEquipmentService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 设备  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/equipment")
@Api(value = "设备接口", description = "设备接口")
public class TEquipmentController extends BaseController<TEquipment, TEquipmentService> {
    @RequiresPermissions("equipment.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询设备", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("equipment.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "设备详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TEquipment param = WebUtil.getParameter(request, TEquipment.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("equipment.update")
    @ApiOperation(value = "修改设备", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TEquipment param = WebUtil.getParameter(request, TEquipment.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("equipment.delete")
    @ApiOperation(value = "删除设备", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TEquipment param = WebUtil.getParameter(request, TEquipment.class);
        return super.delete(param);
    }
}