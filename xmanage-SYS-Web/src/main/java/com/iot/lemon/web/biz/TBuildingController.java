package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TBuilding;
import com.iot.lemon.service.TBuildingService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 楼栋  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/building")
@Api(value = "楼栋接口", description = "楼栋接口")
public class TBuildingController extends BaseController<TBuilding, TBuildingService> {
    @RequiresPermissions("building.read")
    @PutMapping(value = "/read/page")
    @ApiOperation(value = "查询楼栋", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("building.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询楼栋", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryList(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.queryList(param);
    }

    @RequiresPermissions("building.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "楼栋详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TBuilding param = WebUtil.getParameter(request, TBuilding.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("building.update")
    @ApiOperation(value = "修改楼栋", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TBuilding param = WebUtil.getParameter(request, TBuilding.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("building.delete")
    @ApiOperation(value = "删除楼栋", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TBuilding param = WebUtil.getParameter(request, TBuilding.class);
        return super.delete(param);
    }
}