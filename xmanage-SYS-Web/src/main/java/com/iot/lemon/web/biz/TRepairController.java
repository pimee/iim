package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TRepair;
import com.iot.lemon.service.TRepairService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 保修维修  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/repair")
@Api(value = "保修维修接口", description = "保修维修接口")
public class TRepairController extends BaseController<TRepair, TRepairService> {
    @RequiresPermissions("repair.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询保修维修", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("repair.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "保修维修详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TRepair param = WebUtil.getParameter(request, TRepair.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("repair.update")
    @ApiOperation(value = "修改保修维修", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TRepair param = WebUtil.getParameter(request, TRepair.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("repair.delete")
    @ApiOperation(value = "删除保修维修", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TRepair param = WebUtil.getParameter(request, TRepair.class);
        return super.delete(param);
    }
}