package com.iot.lemon.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TAppVersion;
import com.iot.lemon.service.AppVersionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.support.file.fastdfs.FileModel;
import top.ibase4j.core.util.UploadUtil;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 客户端版本管理  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-11-13
 */
@Controller
@RequestMapping("/appVersion")
@Api(value = "客户端版本管理接口", description = "客户端版本管理接口")
public class AppVersionController extends BaseController<TAppVersion, AppVersionService> {
    @RequiresPermissions("appVersion.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询客户端版本管理", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        ModelMap modelMap = new ModelMap();
        return super.query(modelMap, param);
    }

    @RequiresPermissions("appVersion.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "客户端版本管理详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TAppVersion param = WebUtil.getParameter(request, TAppVersion.class);
        ModelMap modelMap = new ModelMap();
        return super.get(modelMap, param);
    }

    @PostMapping
    @RequiresPermissions("appVersion.update")
    @ApiOperation(value = "修改客户端版本管理", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TAppVersion param = WebUtil.getParameter(request, TAppVersion.class);
        String name = param.getUrl();
        String filePath = UploadUtil.getUploadDir(request);
        if (StringUtils.isNotBlank(name) && !name.toLowerCase().contains("http")) {
            FileModel file = UploadUtil.remove2FDFS(filePath + name);
            param.setUrl(file.getRemotePath());
            param.setAppSize(file.getSize());
        }
        ModelMap modelMap = new ModelMap();
        return super.update(modelMap, param);
    }

    @DeleteMapping
    @RequiresPermissions("appVersion.delete")
    @ApiOperation(value = "删除客户端版本管理", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object delete(HttpServletRequest request) {
        TAppVersion param = WebUtil.getParameter(request, TAppVersion.class);
        ModelMap modelMap = new ModelMap();
        return super.delete(modelMap, param);
    }
}
