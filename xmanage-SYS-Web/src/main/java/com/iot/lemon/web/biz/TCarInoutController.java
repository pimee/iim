package com.iot.lemon.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TCarInout;
import com.iot.lemon.service.TCarInoutService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 车辆出入记录  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/carInout")
@Api(value = "车辆出入记录接口", description = "车辆出入记录接口")
public class TCarInoutController extends BaseController<TCarInout, TCarInoutService> {
    @RequiresPermissions("car.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询车辆出入记录", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("car.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "车辆出入记录详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TCarInout param = WebUtil.getParameter(request, TCarInout.class);
        return super.get(param);
    }
}