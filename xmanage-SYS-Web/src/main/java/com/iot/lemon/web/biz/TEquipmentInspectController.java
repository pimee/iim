package com.iot.lemon.web.biz;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TEquipmentInspect;
import com.iot.lemon.service.TEquipmentInspectService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 设备巡检记录  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/equipmentInspect")
@Api(value = "设备巡检记录接口", description = "设备巡检记录接口")
public class TEquipmentInspectController extends BaseController<TEquipmentInspect, TEquipmentInspectService> {
    @RequiresPermissions("equipment.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询设备巡检记录", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        return super.query(param);
    }

    @RequiresPermissions("equipment.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "设备巡检记录详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TEquipmentInspect param = WebUtil.getParameter(request, TEquipmentInspect.class);
        return super.get(param);
    }
}