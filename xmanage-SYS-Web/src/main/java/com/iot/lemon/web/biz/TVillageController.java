package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.SysUser;
import com.iot.lemon.model.TVillage;
import com.iot.lemon.service.SysDicService;
import com.iot.lemon.service.SysUserService;
import com.iot.lemon.service.TVillageService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 小区  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@Controller
@RequestMapping("/village")
@Api(value = "小区接口", description = "小区接口")
public class TVillageController extends BaseController<TVillage, TVillageService> {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private SysDicService sysDicService;

    @RequiresPermissions("village.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询小区", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("managerId", getCurrUser().getId());
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("village.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "小区详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TVillage param = WebUtil.getParameter(request, TVillage.class);
        TVillage result = service.queryById(param.getId());
        Map<String, Object> modelMap = InstanceUtil.newHashMap();
        if (StringUtils.isNotBlank(result.getLocationProvince())) {
            List<?> list = sysDicService.getDics("PROVINCE", result.getLocationProvince(), "CITY");
            modelMap.put("locationCitys", list);
        }
        if (StringUtils.isNotBlank(result.getLocationCity())) {
            List<?> list = sysDicService.getDics("CITY", result.getLocationCity(), "AREA");
            modelMap.put("locationAreas", list);
        }
        modelMap.putAll(InstanceUtil.transBean2Map(result));
        return setSuccessModelMap(modelMap);
    }

    @PostMapping
    @RequiresPermissions("village.update")
    @ApiOperation(value = "修改小区", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TVillage param = WebUtil.getParameter(request, TVillage.class);
        SysUser sysUser = sysUserService.queryById(param.getManagerId());
        param.setOrgaId(sysUser.getOrgaId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("village.delete")
    @ApiOperation(value = "删除小区", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TVillage param = WebUtil.getParameter(request, TVillage.class);
        return super.delete(param);
    }
}
