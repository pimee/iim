package com.iot.lemon.web.sys;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iot.lemon.model.SysOrganization;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.service.SysOrganizationService;
import com.iot.lemon.service.SysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;

/**
 * 组织结构管理控制类
 *
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:13:31
 */
@RestController
@Api(value = "组织结构管理", description = "组织结构管理")
@RequestMapping(value = "organization")
public class SysOrganizationController extends BaseController<SysOrganization, SysOrganizationService> {
    @Autowired
    private SysUserService sysUserService;

    @ApiOperation(value = "查询组织结构")
    @RequiresPermissions("sys.base.organization.read")
    @PutMapping(value = "/read/list")
    public Object list(ModelMap modelMap, @RequestBody Map<String, Object> param) {
        SysUser sysUser = sysUserService.queryById(getCurrUser().getId());
        List<SysOrganization> list = service.getChildren(sysUser.getOrgaId());
        return setSuccessModelMap(list);
    }

    @Override
    @ApiOperation(value = "查询组织结构")
    @RequiresPermissions("sys.base.organization.read")
    @PutMapping(value = "/read/page")
    public Object query(ModelMap modelMap, @RequestBody Map<String, Object> param) {
        return super.query(modelMap, param);
    }

    @ApiOperation(value = "组织结构详情")
    @RequiresPermissions("sys.base.organization.read")
    @PutMapping(value = "/read/detail")
    public Object get(ModelMap modelMap, @RequestBody SysOrganization param) {
        return super.get(modelMap, param);
    }

    @Override
    @PostMapping
    @ApiOperation(value = "修改组织结构")
    @RequiresPermissions("sys.base.organization.update")
    public Object update(ModelMap modelMap, @RequestBody SysOrganization param) {
        return super.update(modelMap, param);
    }

    @Override
    @DeleteMapping
    @ApiOperation(value = "删除组织结构")
    @RequiresPermissions("sys.base.organization.delete")
    public Object delete(ModelMap modelMap, @RequestBody SysOrganization param) {
        return super.delete(modelMap, param);
    }
}
