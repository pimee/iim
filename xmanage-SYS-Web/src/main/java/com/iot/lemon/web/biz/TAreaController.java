package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TArea;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 区域  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/area")
@Api(value = "区域接口", description = "区域接口")
public class TAreaController extends BaseController<TArea, TAreaService> {
    @RequiresPermissions("area.read")
    @PutMapping(value = "/read/page")
    @ApiOperation(value = "查询区域", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPage(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("area.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询区域", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryList(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId());
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.queryList(param);
    }

    @RequiresPermissions("area.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "区域详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TArea param = WebUtil.getParameter(request, TArea.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("area.update")
    @ApiOperation(value = "修改区域", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TArea param = WebUtil.getParameter(request, TArea.class);
        param.setVillageId(UserUtil.getMyVillageId());
        param.setOrgaId(UserUtil.getMyOrgaId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("area.delete")
    @ApiOperation(value = "删除区域", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TArea param = WebUtil.getParameter(request, TArea.class);
        return super.delete(param);
    }
}