package com.iot.lemon.web.biz;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TAttendance;
import com.iot.lemon.service.TAttendanceService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 考勤  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/attendance")
@Api(value = "考勤接口", description = "考勤接口")
public class TAttendanceController extends BaseController<TAttendance, TAttendanceService> {
    @RequiresPermissions("attendance.read")
    @PutMapping(value = "/read/list")
    @ApiOperation(value = "查询考勤", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("villageId", UserUtil.getMyVillageId());
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequiresPermissions("attendance.read")
    @PutMapping(value = "/read/patrolList")
    @ApiOperation(value = "查询考勤", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPatrol(HttpServletRequest request) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("villageId", UserUtil.getMyVillageId());
        List<Long> orgaIds = UserUtil.getMyOrgIds();
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        if (param.get("keyword") == null && param.get("search") != null) {
            param.put("keyword", param.get("search"));
            param.remove("search");
        }
        Object page = service.queryPatrol(param);
        return setSuccessModelMap(page);
    }

    @RequiresPermissions("attendance.read")
    @PutMapping(value = "/read/detail")
    @ApiOperation(value = "考勤详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request) {
        TAttendance param = WebUtil.getParameter(request, TAttendance.class);
        return super.get(param);
    }

    @PostMapping
    @RequiresPermissions("attendance.update")
    @ApiOperation(value = "修改考勤", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TAttendance param = WebUtil.getParameter(request, TAttendance.class);
        param.setOrgaId(UserUtil.getMyOrgaId());
        param.setVillageId(UserUtil.getMyVillageId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("attendance.delete")
    @ApiOperation(value = "删除考勤", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TAttendance param = WebUtil.getParameter(request, TAttendance.class);
        return super.delete(param);
    }
}