package com.iot.lemon.web.biz;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iot.lemon.model.TVillageManager;
import com.iot.lemon.service.TVillageManagerService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.BaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 小区管理员  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@Controller
@RequestMapping("/villageManager")
@Api(value = "小区管理员接口", description = "小区管理员接口")
public class TVillageManagerController extends BaseController<TVillageManager, TVillageManagerService> {
    @PostMapping
    @RequiresPermissions("villageManager.update")
    @ApiOperation(value = "修改小区管理员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request) {
        TVillageManager param = WebUtil.getParameter(request, TVillageManager.class);
        param.setUserId(getCurrUser().getId());
        return super.update(param);
    }

    @DeleteMapping
    @RequiresPermissions("villageManager.delete")
    @ApiOperation(value = "删除小区管理员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request) {
        TVillageManager param = WebUtil.getParameter(request, TVillageManager.class);
        return super.delete(param);
    }
}