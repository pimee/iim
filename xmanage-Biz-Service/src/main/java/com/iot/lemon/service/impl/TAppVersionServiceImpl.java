package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TAppVersionMapper;
import com.iot.lemon.model.TAppVersion;
import com.iot.lemon.service.AppVersionService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 * 客户端版本管理  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-11-13
 */
@CacheConfig(cacheNames = "TAppVersion")
@Service(interfaceClass = AppVersionService.class)
public class TAppVersionServiceImpl extends BaseServiceImpl<TAppVersion, TAppVersionMapper> implements AppVersionService {

}