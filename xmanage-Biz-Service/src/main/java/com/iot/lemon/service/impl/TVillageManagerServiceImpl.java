package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TVillageManagerMapper;
import com.iot.lemon.model.TVillageManager;
import com.iot.lemon.service.TVillageManagerService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 小区管理员 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@CacheConfig(cacheNames = "TVillageManager")
@Service(interfaceClass = TVillageManagerService.class)
public class TVillageManagerServiceImpl extends BaseServiceImpl<TVillageManager, TVillageManagerMapper>
    implements TVillageManagerService {

}
