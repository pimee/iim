package com.iot.lemon.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.THouseMapper;
import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.THouse;
import com.iot.lemon.model.TTenement;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.TBuildingService;
import com.iot.lemon.service.THouseService;
import com.iot.lemon.service.TTenementService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.DataUtil;

/**
 * <p>
 * 房屋 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = THouseService.class)
@CacheConfig(cacheNames = "THouse")
public class THouseServiceImpl extends BaseServiceImpl<THouse, THouseMapper> implements THouseService {
    @Autowired
    private TAreaService areaService;
    @Autowired
    private TBuildingService buildingService;
    @Autowired
    private TTenementService tenementService;

    @Override
    public Pagination<THouse> query(Map<String, Object> params) {
        Pagination<THouse> page = super.query(params);
        for (THouse house : page.getRecords()) {
            TBuilding building = buildingService.queryById(house.getBuildingId());
            if (building != null) {
                house.setHouseType(building.getHouseType());
                house.setBuildingName(building.getBuildingName());
                TArea area = areaService.queryById(building.getAreaId());
                if (area != null) {
                    house.setAreaName(area.getAreaName());
                }
            }
            TTenement tenement = getOwner(house.getId());
            if (tenement != null) {
                house.setOwnerName(tenement.getFullName());
                house.setOwnerCard(tenement.getIdcard());
                house.setOwnerPhone(tenement.getPhone());
            }
        }
        return page;
    }

    @Override
    public THouse queryById(Long id) {
        THouse house = super.queryById(id);
        TTenement tenement = getOwner(id);
        if (tenement != null) {
            house.setOwnerName(tenement.getFullName());
            house.setOwnerCard(tenement.getIdcard());
        }
        TBuilding building = buildingService.queryById(house.getBuildingId());
        if (building != null) {
            house.setAreaId(building.getAreaId());
        }
        return house;
    }

    @Override
    @Transactional
    public THouse update(THouse record) {
        if (record.getManageFee() == null && record.getOwnArea() != null) {
            TBuilding building = buildingService.queryById(record.getBuildingId());
            if (building != null) {
                record.setManageFee(building.getManageFee().multiply(record.getOwnArea()));
            }
        }
        THouse result = super.update(record);
        if (DataUtil.isNotEmpty(record.getOwnerName())) {
            TTenement tenement = getOwner(result.getId());
            if (tenement == null) {
                tenement = new TTenement();
                tenement.setCreateBy(result.getUpdateBy());
            } else if (record.getOwnerPhone() == null) {
                record.setOwnerPhone(tenement.getPhone());
            }
            tenement.setOrgaId(record.getOrgaId()).setVillageId(record.getVillageId()).setHouseId(result.getId())
            .setFullName(record.getOwnerName()).setIdcard(record.getOwnerCard()).setPhone(record.getOwnerPhone())
            .setMentType("1");
            tenement.setUpdateBy(record.getUpdateBy());
            tenement = tenementService.update(tenement);
        }
        return result;
    }

    /** 获取房屋业主 */
    private TTenement getOwner(Long houseId) {
        TTenement entity = new TTenement();
        entity.setHouseId(houseId);
        entity.setMentType("1");
        return tenementService.selectOne(entity);
    }

    @Override
    public void updateManageFee(TArea area) {
        TBuilding param = new TBuilding().setAreaId(area.getId());
        List<TBuilding> list = buildingService.queryList(param);
        for (TBuilding building : list) {
            building.setManageFee(area.getManageFee());
            buildingService.update(building);
            mapper.updateManageFeeByBuilding(building);
        }
    }

    @Override
    public void updateManageFee(TBuilding building) {
        mapper.updateManageFeeByBuilding(building);
    }
}
