package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TRepairMapper;
import com.iot.lemon.model.TRepair;
import com.iot.lemon.service.TRepairService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 保修维修 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TRepairService.class)
@CacheConfig(cacheNames = "TRepair")
public class TRepairServiceImpl extends BaseServiceImpl<TRepair, TRepairMapper> implements TRepairService {

}
