package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TVillageNoticeMapper;
import com.iot.lemon.model.TVillageNotice;
import com.iot.lemon.service.TVillageNoticeService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 小区公告 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-17
 */
@Service(interfaceClass = TVillageNoticeService.class)
@CacheConfig(cacheNames = "TVillageNotice")
public class TVillageNoticeServiceImpl extends BaseServiceImpl<TVillageNotice, TVillageNoticeMapper> implements TVillageNoticeService {

}
