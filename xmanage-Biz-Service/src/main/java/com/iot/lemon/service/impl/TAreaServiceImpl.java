package com.iot.lemon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TAreaMapper;
import com.iot.lemon.model.TArea;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.THouseService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 区域 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TAreaService.class)
@CacheConfig(cacheNames = "TArea")
public class TAreaServiceImpl extends BaseServiceImpl<TArea, TAreaMapper> implements TAreaService {
    @Autowired
    private THouseService houseService;
    @Override
    public TArea update(TArea record) {
        if (record.getManageFee() != null && record.getId() != null) {
            houseService.updateManageFee(record);
        }
        return super.update(record);
    }
}
