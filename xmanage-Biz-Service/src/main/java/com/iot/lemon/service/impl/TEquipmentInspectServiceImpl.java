package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TEquipmentInspectMapper;
import com.iot.lemon.model.TEquipmentInspect;
import com.iot.lemon.service.TEquipmentInspectService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 设备巡检记录 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TEquipmentInspectService.class)
@CacheConfig(cacheNames = "TEquipmentInspect")
public class TEquipmentInspectServiceImpl extends BaseServiceImpl<TEquipmentInspect, TEquipmentInspectMapper> implements TEquipmentInspectService {

}
