package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TCarInoutMapper;
import com.iot.lemon.model.TCarInout;
import com.iot.lemon.service.TCarInoutService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 车辆出入记录 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TCarInoutService.class)
@CacheConfig(cacheNames = "TCarInout")
public class TCarInoutServiceImpl extends BaseServiceImpl<TCarInout, TCarInoutMapper> implements TCarInoutService {

}
