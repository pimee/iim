package com.iot.lemon.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TVillageMapper;
import com.iot.lemon.model.SysOrganization;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.model.TVillage;
import com.iot.lemon.model.TVillageManager;
import com.iot.lemon.service.SysDicService;
import com.iot.lemon.service.SysOrganizationService;
import com.iot.lemon.service.SysUserService;
import com.iot.lemon.service.TVillageManagerService;
import com.iot.lemon.service.TVillageService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.InstanceUtil;

/**
 * <p>
 * 小区 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@CacheConfig(cacheNames = "TVillage")
@Service(interfaceClass = TVillageService.class)
public class TVillageServiceImpl extends BaseServiceImpl<TVillage, TVillageMapper> implements TVillageService {
    @Autowired
    private TVillageManagerService villageManagerService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private SysDicService dicService;
    @Autowired
    private SysOrganizationService sysOrganizationService;

    @Override
    @Transactional
    public TVillage update(TVillage record) {
        boolean insert = record.getId() == null;
        TVillage result = super.update(record);
        if (!insert) {
            TVillageManager manager = new TVillageManager();
            manager.setVillageId(result.getId());
            villageManagerService.deleteByEntity(manager);
        }
        TVillageManager manager = new TVillageManager();
        manager.setVillageId(result.getId());
        manager.setUserId(record.getManagerId());
        villageManagerService.update(manager);
        return result;
    }

    @Override
    public TVillage queryById(Long id) {
        TVillage result = super.queryById(id);
        result.setManagers(InstanceUtil.newArrayList());
        TVillageManager manager = new TVillageManager();
        manager.setVillageId(result.getId());
        List<TVillageManager> list = villageManagerService.queryList(manager);
        for (TVillageManager record : list) {
            SysUser user = userService.queryById(record.getUserId());
            if (user != null) {
                record.setUserName(user.getUserName());
                result.getManagers().add(record);
            }
        }
        return result;
    }

    @Override
    public Pagination<TVillage> query(Map<String, Object> params) {
        Pagination<TVillage> page = super.query(params);
        for (TVillage village : page.getRecords()) {
            if (village.getOrgaId() != null && village.getOrgaId() != 0) {
                SysOrganization sysOrga = sysOrganizationService.queryById(village.getOrgaId());
                if (sysOrga != null) {
                    village.setOrgaName(sysOrga.getOrgaName());
                } else {
                    village.setOrgaId(null);
                }
            }
            TVillageManager manager = new TVillageManager();
            manager.setVillageId(village.getId());
            StringBuilder sb = new StringBuilder();
            List<TVillageManager> list = villageManagerService.queryList(manager);
            for (TVillageManager record : list) {
                SysUser user = userService.queryById(record.getUserId());
                if (user != null) {
                    sb.append(sb.length() > 0 ? "," : "").append(user.getUserName());
                }
            }
            village.setManagerNames(sb.toString());
            if (village.getLocationProvince() != null) {
                String location = dicService.getText("COUNTRY", "ZHCN", "PROVINCE", village.getLocationProvince());
                if (village.getLocationCity() != null) {
                    location += dicService.getText("PROVINCE", village.getLocationProvince(), "CITY",
                        village.getLocationCity());
                    if (village.getLocationArea() != null) {
                        location += dicService.getText("CITY", village.getLocationCity(), "AREA",
                            village.getLocationArea());
                    }
                }
                village.setLocation(location);
            }
        }
        return page;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        super.delete(id);
        TVillageManager manager = new TVillageManager();
        manager.setVillageId(id);
        villageManagerService.deleteByEntity(manager);
    }
}
