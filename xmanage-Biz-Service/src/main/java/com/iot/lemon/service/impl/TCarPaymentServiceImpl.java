package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TCarPaymentMapper;
import com.iot.lemon.model.TCarPayment;
import com.iot.lemon.service.TCarPaymentService;

import top.ibase4j.core.base.BaseServiceImpl;
/**
 * <p>
 * 车辆缴费记录 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TCarPaymentService.class)
@CacheConfig(cacheNames = "TCarPayment")
public class TCarPaymentServiceImpl extends BaseServiceImpl<TCarPayment, TCarPaymentMapper> implements TCarPaymentService {

}
