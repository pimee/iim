package com.iot.lemon.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TEquipmentMapper;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.TEquipment;
import com.iot.lemon.service.SysUserService;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.TBuildingService;
import com.iot.lemon.service.TEquipmentService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
/**
 * <p>
 * 设备 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TEquipmentService.class)
@CacheConfig(cacheNames = "TEquipment")
public class TEquipmentServiceImpl extends BaseServiceImpl<TEquipment, TEquipmentMapper> implements TEquipmentService {
    @Autowired
    private TAreaService areaService;
    @Autowired
    private TBuildingService buildingService;
    @Autowired
    private SysUserService userService;

    @Override
    public Pagination<TEquipment> query(Map<String, Object> params) {
        Pagination<TEquipment> page = super.query(params);
        for (TEquipment equipment : page.getRecords()) {
            TBuilding building = buildingService.queryById(equipment.getBuildingId());
            if (building != null) {
                equipment.setBuildingName(building.getBuildingName());
                TArea area = areaService.queryById(building.getAreaId());
                if (area != null) {
                    equipment.setAreaName(area.getAreaName());
                }
            }
            SysUser sysUser = userService.queryById(building.getManagerId());
            if (sysUser != null) {
                equipment.setInspectorName(sysUser.getUserName());
                equipment.setInspectorPhone(sysUser.getPhone());
            }
        }
        return page;
    }

    @Override
    public TEquipment queryById(Long id) {
        TEquipment equipment = super.queryById(id);
        TBuilding building = buildingService.queryById(equipment.getBuildingId());
        if (building != null) {
            equipment.setAreaId(building.getAreaId());
        }
        return equipment;
    }
}
