package com.iot.lemon.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TAttendanceMapper;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TAttendance;
import com.iot.lemon.model.TVillage;
import com.iot.lemon.service.SysUserService;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.TAttendanceService;
import com.iot.lemon.service.TVillageService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.DateUtil;
import top.ibase4j.core.util.InstanceUtil;

/**
 * <p>
 * 考勤 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TAttendanceService.class)
@CacheConfig(cacheNames = "TAttendance")
public class TAttendanceServiceImpl extends BaseServiceImpl<TAttendance, TAttendanceMapper> implements TAttendanceService {
    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private TVillageService villageService;
    @Autowired
    private TAreaService areaService;

    @Override
    public Pagination<TAttendance> query(Map<String, Object> params) {
        Pagination<TAttendance> page = super.query(params);
        for (TAttendance attendance : page.getRecords()) {
            SysUser sysUser = sysUserService.queryById(attendance.getStaffId());
            if (sysUser != null) {
                attendance.setStaffName(sysUser.getUserName()).setLat(sysUser.getLat()).setLon(sysUser.getLon());
            }
            TVillage village = villageService.queryById(attendance.getVillageId());
            if (village != null) {
                attendance.setVillageName(village.getVillageName());
            }
            if (attendance.getAreaId() != null) {
                TArea area = areaService.queryById(attendance.getAreaId());
                if (area != null) {
                    attendance.setAreaName(area.getAreaName());
                }
            }
        }
        return page;
    }

    @Override
    public List<TAttendance> queryPatrol(Map<String, Object> param) {
        param.put("startTime", DateUtil.getDate());
        List<TAttendance> list = super.queryList(param);
        for (TAttendance attendance : list) {
            SysUser sysUser = sysUserService.queryById(attendance.getStaffId());
            if (sysUser != null) {
                attendance.setStaffName(sysUser.getUserName()).setLat(sysUser.getLat()).setLon(sysUser.getLon());
            }
            TVillage village = villageService.queryById(attendance.getVillageId());
            if (village != null) {
                attendance.setVillageName(village.getVillageName());
            }
            if (attendance.getAreaId() != null) {
                TArea area = areaService.queryById(attendance.getAreaId());
                if (area != null) {
                    attendance.setAreaName(area.getAreaName());
                }
            }
        }
        return list;
    }

    @Override
    public TAttendance update(TAttendance record) {
        Map<String, Object> param = InstanceUtil.newHashMap();
        param.put("startTime", DateUtil.getDate());
        param.put("staffId", record.getStaffId());
        List<TAttendance> list = queryList(param);
        if (!list.isEmpty()) {
            record.setId(list.get(0).getId());
            if (System.currentTimeMillis() - record.getSignTime1().getTime() < 1000 * 60 * 5) {
                throw new RuntimeException("您已经打卡上班.");
            }
            if (record.getSignTime2() != null) {
                throw new RuntimeException("您已经打卡下班.");
            }
            record.setSignTime2(new Date());
        } else {
            record.setSignTime1(new Date());
        }
        return super.update(record);
    }
}
