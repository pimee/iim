package com.iot.lemon.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TCarMapper;
import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.TCar;
import com.iot.lemon.model.THouse;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.TBuildingService;
import com.iot.lemon.service.TCarService;
import com.iot.lemon.service.THouseService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
/**
 * <p>
 * 车辆 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TCarService.class)
@CacheConfig(cacheNames = "TCar")
public class TCarServiceImpl extends BaseServiceImpl<TCar, TCarMapper> implements TCarService {
    @Autowired
    private TAreaService areaService;
    @Autowired
    private TBuildingService buildingService;
    @Autowired
    private THouseService houseService;

    @Override
    public Pagination<TCar> query(Map<String, Object> params) {
        Pagination<TCar> page = super.query(params);
        for (TCar car : page.getRecords()) {
            THouse house = houseService.queryById(car.getHouseId());
            if (house != null) {
                TBuilding building = buildingService.queryById(house.getBuildingId());
                if (building != null) {
                    TArea area = areaService.queryById(building.getAreaId());
                    if (area != null) {
                        car.setHouseName(
                            area.getAreaName() + "-" + building.getBuildingName() + "-" + house.getHouseNo());
                    } else {
                        car.setHouseName(building.getBuildingName() + "-" + house.getHouseNo());
                    }
                } else {
                    car.setHouseName(house.getHouseNo());
                }
            }
        }
        return page;
    }
}
