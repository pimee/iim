package com.iot.lemon.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.TBuildingMapper;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.TEquipment;
import com.iot.lemon.model.THouse;
import com.iot.lemon.service.SysUserService;
import com.iot.lemon.service.TAreaService;
import com.iot.lemon.service.TBuildingService;
import com.iot.lemon.service.TEquipmentService;
import com.iot.lemon.service.THouseService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;
import top.ibase4j.core.util.InstanceUtil;

/**
 * <p>
 * 楼栋 服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Service(interfaceClass = TBuildingService.class)
@CacheConfig(cacheNames = "TBuilding")
public class TBuildingServiceImpl extends BaseServiceImpl<TBuilding, TBuildingMapper> implements TBuildingService {
    @Autowired
    private TAreaService areaService;
    @Autowired
    private SysUserService userService;
    @Autowired
    private TEquipmentService equipmentService;
    @Autowired
    private THouseService houseService;

    @Override
    public Pagination<TBuilding> query(Map<String, Object> params) {
        Pagination<TBuilding> page = super.query(params);
        for (TBuilding building : page.getRecords()) {
            TArea area = areaService.queryById(building.getAreaId());
            if (area != null) {
                building.setAreaName(area.getAreaName());
            }
            if (building.getManagerId() != null) {
                SysUser sysUser = userService.queryById(building.getManagerId());
                if (sysUser != null) {
                    building.setManagerName(sysUser.getUserName());
                    building.setManagerPhone(sysUser.getPhone());
                }
            }
            building.setEquipmentNum(equipmentService.count(new TEquipment().setBuildingId(building.getId())));
            THouse house = new THouse().setBuildingId(building.getId());
            building.setTotalHouse(houseService.count(house));
        }
        return page;
    }

    @Override
    @Transactional
    public TBuilding update(TBuilding record) {
        if (record.getManageFee() == null) {
            TArea area = areaService.queryById(record.getAreaId());
            if (area != null) {
                record.setManageFee(area.getManageFee());
            }
        }
        if (record.getManageFee() != null && record.getId() != null) {
            houseService.updateManageFee(record);
        }
        TBuilding building = super.update(record);
        if (record.getHouseNos() != null) {
            List<THouse> houses = InstanceUtil.newArrayList();
            for (String houseNo : record.getHouseNos()) {
                THouse house = new THouse().setHouseNo(houseNo).setVillageId(record.getVillageId())
                        .setOrgaId(record.getOrgaId()).setBuildingId(building.getId())
                        .setManageFee(building.getManageFee());
                THouse org = houseService.selectOne(house);
                if (org != null) {
                    house.setId(org.getId());
                } else {
                    house.setCreateBy(record.getUpdateBy());
                }
                house.setUpdateBy(record.getUpdateBy());
                houses.add(house);
            }
            houseService.updateBatch(houses);
        }
        return building;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        super.delete(id);
        houseService.deleteByEntity(new THouse().setBuildingId(id));
    }
}
