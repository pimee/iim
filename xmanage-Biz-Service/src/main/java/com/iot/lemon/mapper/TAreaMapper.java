package com.iot.lemon.mapper;

import com.iot.lemon.model.TArea;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TAreaMapper extends BaseMapper<TArea> {

}