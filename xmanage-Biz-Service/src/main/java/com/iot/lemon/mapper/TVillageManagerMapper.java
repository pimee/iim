package com.iot.lemon.mapper;

import com.iot.lemon.model.TVillageManager;

import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
public interface TVillageManagerMapper extends BaseMapper<TVillageManager> {

}