package com.iot.lemon.mapper;

import com.iot.lemon.model.TAppVersion;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-11-13
 */
public interface TAppVersionMapper extends BaseMapper<TAppVersion> {

}