package com.iot.lemon.mapper;

import com.iot.lemon.model.TVillageNotice;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-17
 */
public interface TVillageNoticeMapper extends BaseMapper<TVillageNotice> {

}