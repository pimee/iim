package com.iot.lemon.mapper;

import com.iot.lemon.model.TOrder;
import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-19
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

}