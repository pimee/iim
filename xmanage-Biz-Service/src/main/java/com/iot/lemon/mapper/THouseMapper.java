package com.iot.lemon.mapper;

import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.THouse;

import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface THouseMapper extends BaseMapper<THouse> {
    void updateManageFeeByBuilding(TBuilding building);
}