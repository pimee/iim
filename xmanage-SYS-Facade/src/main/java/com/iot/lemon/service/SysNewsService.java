package com.iot.lemon.service;

import com.iot.lemon.model.SysNews;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysNewsService extends BaseService<SysNews> {

}
