package com.iot.lemon.service;

import com.iot.lemon.model.SysEmail;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailService extends BaseService<SysEmail> {

}
