package com.iot.lemon.service;

import com.iot.lemon.model.SysEmailConfig;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailConfigService extends BaseService<SysEmailConfig> {

}
