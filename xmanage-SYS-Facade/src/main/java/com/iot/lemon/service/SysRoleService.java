package com.iot.lemon.service;

import com.iot.lemon.model.SysRole;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午10:58:43
 */
public interface SysRoleService extends BaseService<SysRole> {
}
