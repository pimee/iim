package com.iot.lemon.service;

import com.iot.lemon.model.SysNotice;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysNoticeService extends BaseService<SysNotice> {

}
