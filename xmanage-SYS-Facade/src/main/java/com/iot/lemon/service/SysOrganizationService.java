package com.iot.lemon.service;

import java.util.List;

import com.iot.lemon.model.SysOrganization;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 组织  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-04
 */
public interface SysOrganizationService extends BaseService<SysOrganization> {
    public List<Long> getChildrenId(Long orgaId);

    public List<SysOrganization> getChildren(Long orgaId);
}