package com.iot.lemon.service;

import com.iot.lemon.model.SysMsg;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年4月24日 上午9:59:39
 */
public interface SysMsgService extends BaseService<SysMsg> {

}
