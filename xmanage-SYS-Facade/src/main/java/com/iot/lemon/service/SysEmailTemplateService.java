package com.iot.lemon.service;

import com.iot.lemon.model.SysEmailTemplate;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 *
 */
public interface SysEmailTemplateService extends BaseService<SysEmailTemplate> {

}
