package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 用户管理
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-04
 */
@ApiModel("用户管理")
@TableName("sys_user")
@SuppressWarnings("serial")
public class SysUser extends BaseModel {

    @ApiModelProperty(value = "登陆帐户")
    @TableField("account_")
    private String account;
    @ApiModelProperty(value = "密码")
    @TableField("password_")
    private String password;
    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "用户类型(1普通用户2系统用户)")
    @TableField("user_type")
    private String userType;
    @ApiModelProperty(value = "姓名")
    @TableField("user_name")
    private String userName;
    @ApiModelProperty(value = "姓名拼音")
    @TableField("name_pinyin")
    private String namePinyin;
    @ApiModelProperty(value = "性别(0:未知;1:男;2:女)")
    @TableField("sex_")
    private Integer sex;
    @ApiModelProperty(value = "头像")
    @TableField("avatar_")
    private String avatar;
    @ApiModelProperty(value = "电话")
    @TableField("phone_")
    private String phone;
    @ApiModelProperty(value = "邮箱")
    @TableField("email_")
    private String email;
    @ApiModelProperty(value = "身份证号码")
    @TableField("id_card")
    private String idCard;
    @ApiModelProperty(value = "微信")
    @TableField("wei_xin")
    private String weiXin;
    @ApiModelProperty(value = "微博")
    @TableField("wei_bo")
    private String weiBo;
    @ApiModelProperty(value = "QQ")
    @TableField("qq_")
    private String qq;
    @ApiModelProperty(value = "出生日期")
    @TableField("birth_day")
    private Date birthDay;
    @ApiModelProperty(value = "职位")
    @TableField("position_")
    private String position;
    @ApiModelProperty(value = "详细地址")
    @TableField("address_")
    private String address;
    @ApiModelProperty(value = "工号")
    @TableField("staff_no")
    private String staffNo;
    @ApiModelProperty(value = "籍贯")
    @TableField("native_place")
    private String nativePlace;
    @ApiModelProperty(value = "从业时间")
    @TableField("working_time")
    private Date workingTime;
    @ApiModelProperty(value = "毕业院校")
    @TableField("graduated_")
    private String graduated;
    @ApiModelProperty(value = "户籍地址")
    @TableField("permanent_address")
    private String permanentAddress;
    @ApiModelProperty(value = "APP会话token")
    @TableField("token_")
    private String token;
    @TableField("lon_")
    private BigDecimal lon;
    @TableField("lat_")
    private BigDecimal lat;

    @TableField(exist = false)
    private String oldPassword;
    @TableField(exist = false)
    private List<Long> orgaIds;
    @TableField(exist = false)
    private String orgaName;
    @TableField(exist = false)
    private String userTypeText;
    @TableField(exist = false)
    private String permission;
    @TableField(exist = false)
    private String userRole;

    public String getAccount() {
        return account;
    }

    public SysUser setAccount(String account) {
        this.account = account;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNamePinyin() {
        return namePinyin;
    }

    public void setNamePinyin(String namePinyin) {
        this.namePinyin = namePinyin;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public SysUser setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getWeiXin() {
        return weiXin;
    }

    public void setWeiXin(String weiXin) {
        this.weiXin = weiXin;
    }

    public String getWeiBo() {
        return weiBo;
    }

    public void setWeiBo(String weiBo) {
        this.weiBo = weiBo;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getStaffNo() {
        return staffNo;
    }

    public void setStaffNo(String staffNo) {
        this.staffNo = staffNo;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getOrgaName() {
        return orgaName;
    }

    public void setOrgaName(String orgaName) {
        this.orgaName = orgaName;
    }

    public String getUserTypeText() {
        return userTypeText;
    }

    public void setUserTypeText(String userTypeText) {
        this.userTypeText = userTypeText;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public List<Long> getOrgaIds() {
        return orgaIds;
    }

    public void setOrgaIds(List<Long> orgaIds) {
        this.orgaIds = orgaIds;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    public Date getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(Date workingTime) {
        this.workingTime = workingTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public String getGraduated() {
        return graduated;
    }

    public void setGraduated(String graduated) {
        this.graduated = graduated;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }
}