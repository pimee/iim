package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 组织
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-04
 */
@ApiModel("组织")
@TableName("sys_organization")
@SuppressWarnings("serial")
public class SysOrganization extends BaseModel {

    @ApiModelProperty(value = "组织名称")
    @TableField("orga_name")
    private String orgaName;
    @ApiModelProperty(value = "组织类型")
    @TableField("orga_type")
    private String orgaType;
    @ApiModelProperty(value = "组织机构码")
    @TableField("orga_code")
    private String orgaCode;
    @TableField("manager_id")
    @ApiModelProperty(value = "负责人")
    private Long managerId;
    @ApiModelProperty(value = "上级组织编号")
    @TableField("parent_id")
    private Long parentId;
    @ApiModelProperty(value = "排序号")
    @TableField("sort_no")
    private Integer sortNo;

    @TableField(exist = false)
    private String parentName;
    @TableField(exist = false)
    private String principal;
    @TableField(exist = false)
    private String managerAccount;
    @TableField(exist = false)
    private String managerName;
    @TableField(exist = false)
    private String managerPhone;
    @TableField(exist = false)
    private String managerPassword;

    public String getOrgaName() {
        return orgaName;
    }

    public void setOrgaName(String orgaName) {
        this.orgaName = orgaName;
    }

    public String getPrincipal() {
        return principal;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public String getOrgaType() {
        return orgaType;
    }

    public void setOrgaType(String orgaType) {
        this.orgaType = orgaType;
    }

    public String getOrgaCode() {
        return orgaCode;
    }

    public void setOrgaCode(String orgaCode) {
        this.orgaCode = orgaCode;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getSortNo() {
        return sortNo;
    }

    public void setSortNo(Integer sortNo) {
        this.sortNo = sortNo;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getManagerAccount() {
        return managerAccount;
    }

    public void setManagerAccount(String managerAccount) {
        this.managerAccount = managerAccount;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public String getManagerPassword() {
        return managerPassword;
    }

    public void setManagerPassword(String managerPassword) {
        this.managerPassword = managerPassword;
    }
}