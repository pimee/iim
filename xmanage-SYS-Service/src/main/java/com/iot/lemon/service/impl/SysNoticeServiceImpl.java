package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysNoticeMapper;
import com.iot.lemon.model.SysNotice;
import com.iot.lemon.service.SysNoticeService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysNotice")
@Service(interfaceClass = SysNoticeService.class)
public class SysNoticeServiceImpl extends BaseServiceImpl<SysNotice, SysNoticeMapper> implements SysNoticeService {

}
