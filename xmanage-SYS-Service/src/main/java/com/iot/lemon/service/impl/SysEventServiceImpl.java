package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysEventMapper;
import com.iot.lemon.service.SysEventService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.model.SysEvent;

@CacheConfig(cacheNames = "sysEvent")
@Service(interfaceClass = SysEventService.class)
public class SysEventServiceImpl extends BaseServiceImpl<SysEvent, SysEventMapper> implements SysEventService {

}
