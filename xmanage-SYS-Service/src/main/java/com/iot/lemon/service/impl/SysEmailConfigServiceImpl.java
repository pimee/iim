package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysEmailConfigMapper;
import com.iot.lemon.model.SysEmailConfig;
import com.iot.lemon.service.SysEmailConfigService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * @author ShenHuaJie
 *
 */
@CacheConfig(cacheNames = "sysEmailConfig")
@Service(interfaceClass = SysEmailConfigService.class)
public class SysEmailConfigServiceImpl extends BaseServiceImpl<SysEmailConfig, SysEmailConfigMapper>
implements SysEmailConfigService {

}
