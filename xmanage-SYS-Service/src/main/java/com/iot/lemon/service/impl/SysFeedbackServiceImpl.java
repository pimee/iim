package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysFeedbackMapper;
import com.iot.lemon.model.SysFeedback;
import com.iot.lemon.service.SysFeedbackService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 * 反馈  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "SysFeedback")
@Service(interfaceClass = SysFeedbackService.class)
public class SysFeedbackServiceImpl extends BaseServiceImpl<SysFeedback, SysFeedbackMapper>
implements SysFeedbackService {

}
