package com.iot.lemon.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.iot.lemon.mapper.SysRoleMapper;
import com.iot.lemon.mapper.SysRoleMenuMapper;
import com.iot.lemon.mapper.SysUserRoleMapper;
import com.iot.lemon.model.SysOrganization;
import com.iot.lemon.model.SysRole;
import com.iot.lemon.model.SysRoleMenu;
import com.iot.lemon.model.SysUserRole;
import com.iot.lemon.service.SysAuthorizeService;
import com.iot.lemon.service.SysOrganizationService;
import com.iot.lemon.service.SysRoleService;

import top.ibase4j.core.base.BaseServiceImpl;
import top.ibase4j.core.support.Pagination;

/**
 * @author ShenHuaJie
 * @version 2016年5月31日 上午11:01:33
 */
@CacheConfig(cacheNames = "sysRole")
@Service(interfaceClass = SysRoleService.class)
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole, SysRoleMapper> implements SysRoleService {
    @Autowired
    private SysOrganizationService sysOrganizationService;
    @Autowired
    private SysAuthorizeService sysAuthorizeService;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;
    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    @Override
    public SysRole queryById(Long id) {
        SysRole sysRole = super.queryById(id);
        if (sysRole != null) {
            if (sysRole.getOrgaId() != null && sysRole.getOrgaId() != 0) {
                SysOrganization organization = sysOrganizationService.queryById(sysRole.getOrgaId());
                if (organization != null) {
                    sysRole.setOrgaName(organization.getOrgaName());
                } else {
                    sysRole.setOrgaId(null);
                }
            }
        }
        return sysRole;
    }

    @Override
    public Pagination<SysRole> query(Map<String, Object> params) {
        Pagination<SysRole> pageInfo = super.query(params);
        // 权限信息
        for (SysRole bean : pageInfo.getRecords()) {
            if (bean.getOrgaId() != null && bean.getOrgaId() != 0) {
                SysOrganization organization = sysOrganizationService.queryById(bean.getOrgaId());
                if (organization != null) {
                    bean.setOrgaName(organization.getOrgaName());
                }
            }
            List<String> permissions = sysAuthorizeService.queryRolePermission(bean.getId());
            for (String permission : permissions) {
                if (StringUtils.isBlank(bean.getPermission())) {
                    bean.setPermission(permission);
                } else {
                    bean.setPermission(bean.getPermission() + ";" + permission);
                }
            }
        }
        return pageInfo;
    }

    @Override
    @Transactional
    public void delete(Long id) {
        super.delete(id);
        sysUserRoleMapper.delete(new EntityWrapper<SysUserRole>(new SysUserRole().setRoleId(id)));
        sysRoleMenuMapper.delete(new EntityWrapper<SysRoleMenu>(new SysRoleMenu().setMenuId(id)));
    }
}
