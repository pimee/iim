package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysArticleMapper;
import com.iot.lemon.model.SysArticle;
import com.iot.lemon.service.SysArticleService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 * 文章  服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "sysArticle")
@Service(interfaceClass = SysArticleService.class)
public class SysArticleServiceImpl extends BaseServiceImpl<SysArticle, SysArticleMapper> implements SysArticleService {

}
