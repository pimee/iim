package com.iot.lemon.service.impl;

import org.springframework.cache.annotation.CacheConfig;

import com.alibaba.dubbo.config.annotation.Service;
import com.iot.lemon.mapper.SysMsgConfigMapper;
import com.iot.lemon.model.SysMsgConfig;
import com.iot.lemon.service.SysMsgConfigService;

import top.ibase4j.core.base.BaseServiceImpl;

/**
 * <p>
 *   服务实现类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-03-12
 */
@CacheConfig(cacheNames = "sysMsgConfig")
@Service(interfaceClass = SysMsgConfigService.class)
public class SysMsgConfigServiceImpl extends BaseServiceImpl<SysMsgConfig, SysMsgConfigMapper>
implements SysMsgConfigService {

}
