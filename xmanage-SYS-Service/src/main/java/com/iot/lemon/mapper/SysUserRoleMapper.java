package com.iot.lemon.mapper;

import com.iot.lemon.model.SysUserRole;

import top.ibase4j.core.base.BaseMapper;

/**
 * @author ShenHuaJie
 * @since 2018年3月3日 下午7:25:07
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
}