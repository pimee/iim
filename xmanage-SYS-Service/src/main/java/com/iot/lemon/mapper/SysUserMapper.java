package com.iot.lemon.mapper;

import com.iot.lemon.model.SysUser;

import top.ibase4j.core.base.BaseMapper;

/**
 * @author ShenHuaJie
 * @since 2018年3月3日 下午7:24:50
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
}
