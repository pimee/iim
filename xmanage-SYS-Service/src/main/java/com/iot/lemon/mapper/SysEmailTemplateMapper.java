package com.iot.lemon.mapper;

import com.iot.lemon.model.SysEmailTemplate;

import top.ibase4j.core.base.BaseMapper;

/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-01-29
 */
public interface SysEmailTemplateMapper extends BaseMapper<SysEmailTemplate> {

}