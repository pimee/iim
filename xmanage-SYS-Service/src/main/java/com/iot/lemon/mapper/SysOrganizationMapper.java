package com.iot.lemon.mapper;

import com.iot.lemon.model.SysOrganization;

import top.ibase4j.core.base.BaseMapper;
/**
 * <p>
 * Mapper接口
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-04
 */
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {

}