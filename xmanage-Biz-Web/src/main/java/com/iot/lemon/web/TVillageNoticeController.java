package com.iot.lemon.web;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iot.lemon.model.TVillageNotice;
import com.iot.lemon.service.TVillageNoticeService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 小区公告  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-17
 */
@Controller
@RequestMapping("/app/villageNotice")
@Api(value = "小区公告接口", description = "小区公告接口")
public class TVillageNoticeController extends AppBaseController<TVillageNotice, TVillageNoticeService> {
    @RequestMapping(value = "/read/list", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "查询小区公告", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, String noticeType,
        String keyword) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("villageId", UserUtil.getMyVillageId(request));
        return super.query(param);
    }

    @RequestMapping(value = "/read/detail", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "小区公告详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, Long id) {
        TVillageNotice param = WebUtil.getParameter(request, TVillageNotice.class);
        param.setVillageId(UserUtil.getMyVillageId(request));
        return super.get(param);
    }

    @PostMapping
    @ApiOperation(value = "修改/发布小区公告", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object update(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, Long id, String noticeType,
        String noticeTitle, String content) {
        TVillageNotice param = WebUtil.getParameter(request, TVillageNotice.class);
        param.setVillageId(UserUtil.getMyVillageId(request));
        return super.update(request, param);
    }

    @DeleteMapping
    @ApiOperation(value = "删除小区公告", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object del(HttpServletRequest request, Long id) {
        TVillageNotice param = WebUtil.getParameter(request, TVillageNotice.class);
        return super.delete(param);
    }
}
