package com.iot.lemon.web;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iot.lemon.model.TCar;
import com.iot.lemon.model.TCarInout;
import com.iot.lemon.service.TCarInoutService;
import com.iot.lemon.service.TCarService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 车辆  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/app/car/")
@Api(value = "车辆接口", description = "车辆接口")
public class TCarController extends AppBaseController<TCar, TCarService> {
    @Autowired
    private TCarInoutService carInoutService;

    @ApiOperation(value = "根据车牌号获取车辆信息", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(value = "/detail", method = {RequestMethod.GET, RequestMethod.POST})
    public Object get(HttpServletRequest request, String plateNumber) {
        TCar param = WebUtil.getParameter(request, TCar.class);
        param.setOrgaId(UserUtil.getMyOrgaId(request));
        param.setVillageId(UserUtil.getMyVillageId(request));
        return super.get(param);
    }

    @ApiOperation(value = "新增车辆出入记录")
    @PostMapping(value = "inout")
    public Object updateInout(HttpServletRequest request, String plateNumber, String inOut, BigDecimal money,
        String door) {
        TCarInout record = WebUtil.getParameter(request, TCarInout.class);
        carInoutService.update(record);
        return setSuccessModelMap();
    }

    @ApiOperation(value = "车位续费")
    @PostMapping(value = "fee")
    public Object updateFee(HttpServletRequest request, String plateNumber, String inOut, BigDecimal money,
        String door) {
        TCarInout record = WebUtil.getParameter(request, TCarInout.class);
        carInoutService.update(record);
        return setSuccessModelMap();
    }
}
