package com.iot.lemon.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iot.lemon.model.TAttendance;
import com.iot.lemon.service.TAttendanceService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 考勤  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/app/attendance")
@Api(value = "考勤接口", description = "考勤接口")
public class TAttendanceController extends AppBaseController<TAttendance, TAttendanceService> {
    @RequestMapping(value = "/read/list", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "查询考勤记录", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, String keyword) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("villageId", UserUtil.getMyVillageId(request));
        param.put("staffId", getCurrUser(request));
        List<Long> orgaIds = UserUtil.getMyOrgIds(request);
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequestMapping(value = "/read/patrolList", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "查询当值人员", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object queryPatrol(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, String keyword) {
        Map<String, Object> param = WebUtil.getParameter(request);
        param.put("villageId", UserUtil.getMyVillageId(request));
        List<Long> orgaIds = UserUtil.getMyOrgIds(request);
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        Object page = service.queryPatrol(param);
        return setSuccessModelMap(page);
    }

    @ApiOperation(value = "打卡")
    @PostMapping(value = "update")
    public Object update(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token) {
        TAttendance param = WebUtil.getParameter(request, TAttendance.class);
        param.setStaffId(getCurrUser(request));
        param.setOrgaId(UserUtil.getMyOrgaId(request));
        param.setVillageId(UserUtil.getMyVillageId(request));
        return super.update(request, param);
    }
}
