package com.iot.lemon.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iot.lemon.model.TTenement;
import com.iot.lemon.service.TTenementService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import top.ibase4j.core.Constants;
import top.ibase4j.core.Constants.MSGCHKTYPE;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.exception.LoginException;
import top.ibase4j.core.support.Assert;
import top.ibase4j.core.support.http.SessionUser;
import top.ibase4j.core.support.security.coder.HmacCoder;
import top.ibase4j.core.util.CacheUtil;
import top.ibase4j.core.util.DataUtil;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.PropertiesUtil;
import top.ibase4j.core.util.SecurityUtil;

/**
 * 用户登录
 *
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:11:21
 */
@RestController
@RequestMapping("/app/ten/")
@Api(value = "APP住户登录注册接口", description = "APP-住户登录注册接口")
public class TenementLoginController extends AppBaseController<TTenement, TTenementService> {
    @PostMapping("reginit.api")
    @ApiOperation(value = "注册", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "使用手机号+验证码进行注册或登录\n"
            + "注册接口需要以下3个参数：\n" + "1. account: 用户名，目前只支持手机号\n" + "2. password: 密码\n"
            + "3. authCode: 手机短信验证码(通过/app/msg.api发送短信验证码)\n" + "注意：所有接口都需要传sign参数，sign用作签名", response = TTenement.class)
    public Object register(@RequestHeader("sign") @ApiParam(value = "签名", required = true) String sign,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password,
        @RequestParam @ApiParam(value = "手机验证码", required = true) String authCode) throws Exception {
        String authCodeOnServer = (String)CacheUtil.getCache().get(MSGCHKTYPE.REGISTER + account);
        if (!authCode.equals(authCodeOnServer)) {
            logger.warn(account + "=" + authCode + "-" + authCodeOnServer);
            throw new IllegalArgumentException("手机验证码错误");
        }

        Map<String, Object> params = InstanceUtil.newHashMap("loginKey", account);
        List<TTenement> members = service.queryList(params);
        TTenement member = members.isEmpty() ? null : members.get(0);

        if (member == null) {
            TTenement param = new TTenement();
            param.setPhone(account);
            param.setPassword(SecurityUtil.encryptPassword(password));
            param.setAvatar(PropertiesUtil.getString("ui.file.uri.prefix") + "extends/img/dftAvatar.png");
            service.update(param);
            return setSuccessModelMap();
        } else {
            throw new IllegalArgumentException("手机号已注册.");
        }
    }

    @PostMapping("login.api")
    @ApiOperation(value = "登录", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "使用手机号+密码登录\n"
            + "登录接口需要以下3个参数：\n" + "1. sign: 请求参数的签名，用于防止请求参数被第三方拦截篡改，每接口必传。\n" + "2. account: 用户名，目前只支持手机号\n"
            + "3. password: 密码\n" + "注意：所有接口都需要传sign参数，sign用作签名", response = TTenement.class)
    public Object login(@RequestHeader("sign") @ApiParam(value = "签名", required = true) String sign,
        @RequestHeader(required = false) @ApiParam("微信ID，暂不支持") String openId,
        @RequestHeader(required = false) @ApiParam("极光推送ID，暂不支持") String registrationId,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password) {
        Map<String, Object> params = InstanceUtil.newHashMap("enable", 1);
        params.put("loginKey", account);
        List<TTenement> members = service.queryList(params);
        TTenement member = members.isEmpty() ? null : members.get(0);

        if (member == null) {
            throw new LoginException("手机号或密码错误.");
        } else {
            if (SecurityUtil.encryptPassword(password).equals(member.getPassword())) {
                // if (member.getRegistrationId() != null) {
                // if (registrationId != null && !registrationId.equals(member.getRegistrationId())) {
                // member.setRegistrationId(registrationId);
                // String content = "帐号[" + account + "]在别的设备登录";
                // HashMap<String, String> hashMap = new HashMap<String, String>();
                // hashMap.put("type", "3");
                // String equipment = member.getRegistrationId().substring(2, 3);
                // try {
                // if ("0".equals(equipment)) {
                // jpushHelper.sendNotificationAndroid("登录通知", content, hashMap,
                // member.getRegistrationId());
                // } else {
                // jpushHelper.sendNotificationIOS("登录通知", content, hashMap, member.getRegistrationId());
                // }
                // } catch (Exception e) {
                // logger.info(ExceptionUtil.getStackTraceAsString(e));
                // }
                // }
                // } else {
                // member.setRegistrationId(registrationId);
                // }
                // if (openId != null) {
                // member.setWxOpenId(openId);
                // }
                member.setIsOnline(1);
                String token = SecurityUtil.initHmacKey(HmacCoder.MD5);
                String tokenKey = SecurityUtil.encryptMd5(token);
                CacheUtil.getCache().set(Constants.TOKEN_KEY + tokenKey,
                    new SessionUser(member.getId(), member.getFullName(), member.getPhone(), false),
                    PropertiesUtil.getInt("APP-TOKEN-EXPIRE", 60 * 60 * 24 * 5));
                member.setToken(token);
                member.setPassword(null);
                service.update(member);
                return setSuccessModelMap(member);
            } else {
                throw new LoginException("手机号或密码错误.");
            }
        }
    }

    @PostMapping("logout.api")
    @ApiOperation(value = "APP会员登出", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object logout(HttpServletRequest request) {
        String token = request.getHeader("token");
        Assert.notNull(token, "ACCOUNT");
        if (StringUtils.isNotBlank(token)) {
            String tokenKey = SecurityUtil.encryptMd5(token);
            CacheUtil.getCache().del(Constants.TOKEN_KEY + tokenKey);
        }
        Long id = getCurrUser(request);
        if (DataUtil.isNotEmpty(id)) {
            TTenement member = new TTenement();
            member.setId(getCurrUser(request));
            member.setIsOnline(0);
            service.update(member);
        }
        ModelMap modelMap = new ModelMap();
        return setSuccessModelMap(modelMap);
    }

    @PostMapping("updatePwd.api")
    @ApiOperation(value = "修改密码", produces = MediaType.APPLICATION_JSON_VALUE, notes = "" + "接口需要以下4个参数：\n"
            + "1. sign: 请求参数的签名，用于防止请求参数被第三方拦截篡改，每接口必传。\n" + "2. account: 用户名，目前只支持手机号\n" + "3. password: 密码\n"
            + "4. authCode: 手机短信验证码(通过/app/msg.api发送短信验证码)\n" + "注意：所有接口都需要传sign参数，sign用作签名", response = TTenement.class)
    public Object updatePwd(@RequestHeader("sign") @ApiParam(value = "签名", required = true) String sign,
        @RequestParam @ApiParam(value = "手机号", required = true) String account,
        @RequestParam @ApiParam(value = "密码", required = true) String password,
        @RequestParam @ApiParam(value = "手机验证码", required = true) String authCode) throws Exception {
        String authCodeOnServer = (String)CacheUtil.getCache().get(MSGCHKTYPE.CHGPWD + account);
        if (!authCode.equals(authCodeOnServer)) {
            throw new IllegalArgumentException("手机验证码错误");
        }

        Map<String, Object> params = InstanceUtil.newHashMap("loginKey", account);
        List<?> members = service.queryList(params);
        TTenement member = members.isEmpty() ? null : (TTenement)members.get(0);

        if (member == null) {
            throw new IllegalArgumentException("手机号还没有注册.");
        } else {
            member.setPassword(SecurityUtil.encryptPassword(password));
            service.update(member);
            return setSuccessModelMap();
        }
    }
}
