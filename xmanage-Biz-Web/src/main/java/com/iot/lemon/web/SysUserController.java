/**
 *
 */
package com.iot.lemon.web;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iot.lemon.bean.Member;
import com.iot.lemon.model.MemberPhoto;
import com.iot.lemon.model.SysUser;
import com.iot.lemon.service.SysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.exception.UnauthorizedException;
import top.ibase4j.core.support.Assert;
import top.ibase4j.core.util.DataUtil;
import top.ibase4j.core.util.InstanceUtil;
import top.ibase4j.core.util.SecurityUtil;
import top.ibase4j.core.util.UploadUtil;
import top.ibase4j.core.util.WebUtil;

/**
 * APP员工信息管理控制器
 *
 * @author ShenHuaJie
 * @version 2016年5月20日 下午3:12:12
 */
@RestController
@Api(value = "APP员工信息", description = "APP员工信息")
@RequestMapping(value = "/app/user/")
public class SysUserController extends AppBaseController<SysUser, SysUserService> {
    @ApiOperation(value = "当前用户信息")
    @RequestMapping(value = "current.api", method = {RequestMethod.GET, RequestMethod.POST})
    public Object current(HttpServletRequest request) {
        SysUser result = service.queryById(getCurrUser(request));
        result.setPassword(null);
        return setSuccessModelMap(result);
    }

    @ApiOperation(value = "获取个人信息", produces = MediaType.APPLICATION_JSON_VALUE, response = SysUser.class)
    @RequestMapping(value = "getUserInfo.api", method = {RequestMethod.GET, RequestMethod.POST})
    public Object get(HttpServletRequest request, String id) {
        Member param = WebUtil.getParameter(request, Member.class);
        Long memberId = getCurrUser(request);
        if (DataUtil.isNotEmpty(memberId)) {
            param.setId(memberId);
        }
        Assert.notNull(param.getId(), "ID");
        SysUser result = service.queryById(param.getId());
        result.setPassword(null);
        ModelMap modelMap = new ModelMap();
        return setSuccessModelMap(modelMap, result);
    }

    @ApiOperation(value = "修改个人信息")
    @PostMapping(value = "update/info")
    public Object updatePerson(HttpServletRequest request, SysUser param) {
        param = WebUtil.getParameter(request, SysUser.class);
        param.setId(getCurrUser(request));
        param.setPassword(null);
        Assert.isNotBlank(param.getAccount(), "ACCOUNT");
        Assert.length(param.getAccount(), 3, 15, "ACCOUNT");
        return super.update(request, param);
    }

    @ApiOperation(value = "修改用户头像")
    @PostMapping(value = "upload/avatar")
    public Object updateAvatar(HttpServletRequest request, MemberPhoto param) {
        Long id = getCurrUser(request);
        if (DataUtil.isNotEmpty(id)) {
            param.setMemberId(id);
        }
        Assert.notNull(param.getMemberId(), "ID");
        List<String> avatars = UploadUtil.uploadImage(request, false);
        org.springframework.util.Assert.notEmpty(avatars, "头像数据dataFile不能为空");
        SysUser sysUser = new SysUser();
        sysUser.setId(param.getMemberId());
        SysUser user = service.queryById(sysUser.getId());
        Assert.notNull(user, "USER", sysUser.getId());
        String filePath = UploadUtil.getUploadDir(request) + avatars.get(0);
        String avatar = UploadUtil.remove2FDFS(filePath).getRemotePath();
        sysUser.setAvatar(avatar);
        Long userId = getCurrUser(request);
        sysUser.setUpdateBy(userId);
        sysUser.setUpdateTime(new Date());
        service.update(sysUser);
        Map<String, Object> result = InstanceUtil.newHashMap("bizeCode", 1);
        result.put("avatar", avatar);
        return setSuccessModelMap(new ModelMap(), result);
    }

    // 修改密码
    @ApiOperation(value = "修改密码")
    @PostMapping(value = "/update/password")
    public Object updatePassword(HttpServletRequest request, String password, String oldPassword) {
        SysUser param = WebUtil.getParameter(request, SysUser.class);
        Assert.isNotBlank(param.getOldPassword(), "OLDPASSWORD");
        Assert.isNotBlank(param.getPassword(), "PASSWORD");
        String encryptPassword = SecurityUtil.encryptPassword(param.getOldPassword());
        SysUser sysUser = service.queryById(getCurrUser(request));
        Assert.notNull(sysUser, "USER", param.getId());
        if (!sysUser.getPassword().equals(encryptPassword)) {
            throw new UnauthorizedException("原密码错误.");
        }
        sysUser.setPassword(SecurityUtil.encryptPassword(param.getPassword()));
        return super.update(request, sysUser);
    }
}
