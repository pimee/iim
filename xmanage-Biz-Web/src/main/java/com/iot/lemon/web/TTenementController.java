package com.iot.lemon.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.iot.lemon.model.TTenement;
import com.iot.lemon.service.TTenementService;
import com.iot.lemon.util.UserUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import top.ibase4j.core.base.AppBaseController;
import top.ibase4j.core.util.WebUtil;

/**
 * <p>
 * 住户  前端控制器
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@Controller
@RequestMapping("/app/tenement")
@Api(value = "住户接口", description = "住户接口")
public class TTenementController extends AppBaseController<TTenement, TTenementService> {
    @RequestMapping(value = "/read/list", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "查询住户", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object query(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, String keyword) {
        Map<String, Object> param = WebUtil.getParameter(request);
        try {
            param.put("villageId", UserUtil.getMyVillageId(request));
        } catch (Exception e) {
        }
        List<Long> orgaIds = UserUtil.getMyOrgIds(request);
        if (orgaIds.size() > 1) {
            param.put("orgaIds", orgaIds);
        }
        return super.query(param);
    }

    @RequestMapping(value = "/read/detail", method = {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "住户详情", produces = MediaType.APPLICATION_JSON_VALUE)
    public Object get(HttpServletRequest request,
        @RequestHeader("token") @ApiParam(value = "token", required = true) String token, Long id) {
        TTenement param = WebUtil.getParameter(request, TTenement.class);
        return super.get(param);
    }
}