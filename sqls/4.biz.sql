-- --------------------------------------------------------
-- 主机:                           101.37.201.70
-- 服务器版本:                        5.7.18-log - Source distribution
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.4.0.5143
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 xmanage_biz 的数据库结构
CREATE DATABASE IF NOT EXISTS `xmanage_biz` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `xmanage_biz`;

-- 导出  表 xmanage_biz.sys_lock 结构
CREATE TABLE IF NOT EXISTS `sys_lock` (
  `key_` varchar(128) NOT NULL,
  `name_` varchar(128) NOT NULL,
  `expire_second` int(6) NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_`),
  KEY `expire_` (`expire_second`),
  KEY `create_time` (`create_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_area 结构
CREATE TABLE IF NOT EXISTS `t_area` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `area_name` varchar(128) NOT NULL,
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `village_id_area_name` (`village_id`,`area_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='区域';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_building 结构
CREATE TABLE IF NOT EXISTS `t_building` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `area_id` bigint(20) NOT NULL COMMENT '区域编号',
  `building_name` varchar(32) NOT NULL COMMENT '楼栋名称',
  `manager_id` bigint(20) DEFAULT NULL COMMENT '物业经理',
  `house_type` varchar(2) DEFAULT '1' COMMENT '房屋类型(1住宅2别墅3写字楼4商铺5自住型商品房)',
  `elevator_num` int(2) DEFAULT NULL COMMENT '电梯数',
  `total_floor` int(3) DEFAULT NULL COMMENT '总楼层',
  `floor_hourse` int(3) DEFAULT NULL COMMENT '每层户数',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `orga_id_village_id_area_id_building_name` (`orga_id`,`village_id`,`area_id`,`building_name`),
  KEY `orga_id` (`orga_id`),
  KEY `village_id` (`village_id`),
  KEY `area_id` (`area_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='楼栋';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_car 结构
CREATE TABLE IF NOT EXISTS `t_car` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `house_id` bigint(20) NOT NULL COMMENT '房屋编号',
  `plate_number` varchar(10) NOT NULL COMMENT '车牌号',
  `car_type` varchar(32) NOT NULL COMMENT '车辆型号',
  `link_man` varchar(32) DEFAULT NULL COMMENT '联系人',
  `link_phone` varchar(32) DEFAULT NULL COMMENT '联系人电话',
  `carport_type` varchar(2) DEFAULT NULL COMMENT '车位类型',
  `carport_no` varchar(10) DEFAULT NULL COMMENT '车位编号',
  `car_status` varchar(1) DEFAULT NULL COMMENT '车辆状态',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `plate_number` (`plate_number`),
  KEY `hourse_id` (`house_id`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='车辆';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_car_inout 结构
CREATE TABLE IF NOT EXISTS `t_car_inout` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_id` bigint(20) NOT NULL COMMENT '车辆编号',
  `in_out` varchar(10) NOT NULL COMMENT '类型',
  `money_` decimal(6,2) DEFAULT NULL COMMENT '金额',
  `in_out_door` varchar(32) DEFAULT NULL COMMENT '出入口',
  `in_out_time` timestamp NULL DEFAULT NULL COMMENT '出入时间',
  `order_no` varchar(64) DEFAULT NULL COMMENT '订单号',
  `pay_way` varchar(64) DEFAULT NULL COMMENT '缴费方式',
  `park_model` varchar(10) DEFAULT NULL COMMENT '停车模式',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `car_id` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='车辆出入记录';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_car_payment 结构
CREATE TABLE IF NOT EXISTS `t_car_payment` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `car_id` bigint(20) NOT NULL COMMENT '车辆编号',
  `money_` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '金额',
  `pay_type` varchar(10) NOT NULL COMMENT '缴费方式',
  `pay_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '缴费时间',
  `order_no` varchar(64) DEFAULT NULL COMMENT '订单号',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `car_id` (`car_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='车辆缴费记录';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_equipment 结构
CREATE TABLE IF NOT EXISTS `t_equipment` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `building_id` bigint(20) NOT NULL COMMENT '楼栋编号',
  `name_` varchar(128) NOT NULL COMMENT '设备名称',
  `no_` varchar(64) DEFAULT NULL COMMENT '设备编号',
  `type_` varchar(2) NOT NULL COMMENT '设备类型',
  `status_` varchar(2) NOT NULL COMMENT '设备状态',
  `inspector_id` bigint(20) DEFAULT NULL COMMENT '巡检人员',
  `last_inspect_time` timestamp NULL DEFAULT NULL COMMENT '最后巡检时间',
  `location_` varchar(256) DEFAULT NULL COMMENT '存放位置',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `building_id` (`building_id`),
  KEY `name_` (`name_`),
  KEY `no_` (`no_`),
  KEY `type_` (`type_`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='设备';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_equipment_inspect 结构
CREATE TABLE IF NOT EXISTS `t_equipment_inspect` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `equipment_id` bigint(20) NOT NULL COMMENT '设备编号',
  `inspector_` varchar(32) NOT NULL COMMENT '巡检人',
  `inspect_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '巡检时间',
  `equipment_status` varchar(10) NOT NULL COMMENT '设备状态',
  `work_order` varchar(32) DEFAULT NULL COMMENT '工单',
  `order_status` varchar(5) DEFAULT NULL COMMENT '工单状态',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `equipment_id` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='设备巡检记录';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_house 结构
CREATE TABLE IF NOT EXISTS `t_house` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `building_id` bigint(20) NOT NULL COMMENT '楼栋编号',
  `house_no` varchar(64) NOT NULL COMMENT '房屋编号',
  `own_area` decimal(5,2) DEFAULT NULL COMMENT '产权面积',
  `manager_id` bigint(20) DEFAULT NULL COMMENT '物业经理',
  `house_status` varchar(2) NOT NULL DEFAULT '1' COMMENT '房屋状态(1空置2装修中3自住4出租)',
  `own_status` varchar(2) NOT NULL DEFAULT '1' COMMENT '产权状态(0未办理1已办理)',
  `bedroom` int(2) DEFAULT NULL COMMENT '卧室数',
  `livingroom` int(2) DEFAULT NULL COMMENT '客厅数',
  `toilet_` int(2) DEFAULT NULL COMMENT '卫生间数',
  `central_heating` varchar(1) DEFAULT '1' COMMENT '是否集中供暖',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `building_id_house_no` (`building_id`,`house_no`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='房屋';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_patrol 结构
CREATE TABLE IF NOT EXISTS `t_patrol` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `area_id` bigint(20) NOT NULL COMMENT '所属区域',
  `staff_id` bigint(20) NOT NULL COMMENT '员工编号',
  `start_time` datetime NOT NULL COMMENT '上班时间',
  `end_time` datetime NOT NULL COMMENT '下班时间',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='巡逻';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_repair 结构
CREATE TABLE IF NOT EXISTS `t_repair` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `work_no` varchar(32) NOT NULL COMMENT '工单号',
  `work_type` varchar(2) NOT NULL COMMENT '类型',
  `work_fee` decimal(10,2) DEFAULT NULL COMMENT '费用',
  `work_detail` varchar(256) DEFAULT NULL COMMENT '详情',
  `disposer_id` bigint(64) DEFAULT NULL COMMENT '处置人',
  `accept_time` timestamp NULL DEFAULT NULL COMMENT '接受时间',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `work_no` (`work_no`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='保修维修';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_staff 结构
CREATE TABLE IF NOT EXISTS `t_staff` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `staff_no` varchar(32) NOT NULL COMMENT '工号',
  `staff_name` varchar(32) NOT NULL COMMENT '姓名',
  `staff_phone` varchar(20) NOT NULL COMMENT '手机号',
  `password_` varchar(64) NOT NULL COMMENT '密码',
  `avatar_` varchar(512) DEFAULT NULL COMMENT '头像',
  `idcard_` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `sex_` varchar(1) DEFAULT NULL COMMENT '性别',
  `birth_day` date DEFAULT NULL COMMENT '出生日期',
  `address_` varchar(256) DEFAULT NULL COMMENT '住址',
  `is_online` varchar(1) DEFAULT '0' COMMENT '是否在线',
  `lon_` decimal(10,7) DEFAULT NULL COMMENT '经度',
  `lat_` decimal(10,7) DEFAULT NULL COMMENT '纬度',
  `token_` varchar(256) DEFAULT NULL COMMENT 'APP会话token',
  `registration_id` varchar(255) DEFAULT NULL,
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `orga_id_staff_no` (`orga_id`,`staff_no`),
  UNIQUE KEY `idcard_` (`idcard_`),
  KEY `staff_name` (`staff_name`),
  KEY `staff_phone` (`staff_phone`),
  KEY `village_id` (`village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='员工';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_staff_post 结构
CREATE TABLE IF NOT EXISTS `t_staff_post` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `staff_id` bigint(20) NOT NULL,
  `post_type` varchar(2) NOT NULL,
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `staff_id_post_type` (`staff_id`,`post_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='员工职位';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_tenement 结构
CREATE TABLE IF NOT EXISTS `t_tenement` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `house_id` bigint(20) NOT NULL COMMENT '房屋编号',
  `full_name` varchar(32) NOT NULL COMMENT '姓名',
  `phone_` varchar(32) DEFAULT NULL COMMENT '联系方式',
  `ment_type` varchar(2) NOT NULL COMMENT '类型(1业主2住户3租客)',
  `idcard_` varchar(18) DEFAULT NULL COMMENT '身份证号',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  KEY `orga_id_village_id` (`orga_id`,`village_id`),
  KEY `house_id_ment_type` (`house_id`,`ment_type`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='住户';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_village 结构
CREATE TABLE IF NOT EXISTS `t_village` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `orga_id` bigint(20) NOT NULL COMMENT '组织编号',
  `village_name` varchar(64) NOT NULL COMMENT '小区名称',
  `location_province` varchar(6) DEFAULT NULL COMMENT '所在省市',
  `location_city` varchar(6) DEFAULT NULL COMMENT '所在市',
  `location_area` varchar(6) DEFAULT NULL COMMENT '所在区',
  `location_detail` varchar(256) DEFAULT NULL COMMENT '详细地址',
  `build_time` int(4) DEFAULT NULL COMMENT '建成年份',
  `remark_` varchar(100) DEFAULT NULL COMMENT '备注',
  `enable_` tinyint(1) DEFAULT '1',
  `create_by` bigint(20) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_by` bigint(20) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `village_name` (`village_name`,`orga_id`),
  KEY `orga_id` (`orga_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='小区';

-- 数据导出被取消选择。
-- 导出  表 xmanage_biz.t_village_manager 结构
CREATE TABLE IF NOT EXISTS `t_village_manager` (
  `id_` bigint(20) NOT NULL AUTO_INCREMENT,
  `village_id` bigint(20) NOT NULL COMMENT '小区编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `enable_` tinyint(1) NOT NULL DEFAULT '1',
  `remark_` varchar(5000) DEFAULT NULL,
  `create_by` bigint(20) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` bigint(20) DEFAULT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_`),
  UNIQUE KEY `user_id_role_id` (`user_id`),
  KEY `village_id` (`village_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='小区管理员';

-- 数据导出被取消选择。
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
