require(['config'],function(){
	require(['jquery', 'bootstrap'], function ($, undefined) {
	    //初始配置
	    var Config = requirejs.s.contexts._.config.config;
	    //将Config渲染到全局
	    window.Config = Config;
	    // 配置语言包的路径
	    var paths = {};
	    paths['utilback'] = 'extends/js/util-back';
	    paths['lang'] = 'extends/js/lang';
	    //如果语言包想要动态加载，则使用下面一行
	    //paths['lang'] = Config.moduleurl + 'lang?callback=define&controllername=' + Config.controllername;
	    require.config({paths: paths});
	
	    // 初始化
	    $(function () {
	        require(['util', 'utilback'], function (Util, Backend) {
                // 错误提示居中显示
                Util.config.toastr.positionClass = "toast-top-center";
                //加载相应模块
                if (Config.jsname) {
                    require([Config.jsname], function (Controller) {
                        Controller[Config.actionname] != undefined && Controller[Config.actionname]();
                    }, function (e) {
                        console.error(e);
                        // 这里可捕获模块加载的错误
                    });
                }
	        });
	    });
	});
});