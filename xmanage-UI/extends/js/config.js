define(function(){
	/**
     * List of all the available skins
    *
    * @type Array
    */
   window.my_skins = [
       "skin-blue",
       "skin-black",
       "skin-red",
       "skin-yellow",
       "skin-purple",
       "skin-green",
       "skin-blue-light",
       "skin-black-light",
       "skin-red-light",
       "skin-yellow-light",
       "skin-purple-light",
       "skin-green-light"
   ];
	require.config({
	    urlArgs: "v=" + requirejs.s.contexts._.config.config.site.version,
	    packages: [{
	            name: 'moment',
	            location: 'assets/moment',
	            main: 'moment'
	        }],
	    paths: {
	        'lang': "empty:",
		    'util': 'extends/js/util',
	        'form': 'extends/js/plugins/form',
	        'table': 'extends/js/plugins/table',
	        'upload': 'extends/js/plugins/upload',
	        'validator': 'extends/js/plugins/validator',
	        'drag': 'assets/jquery.drag.min',
	        'drop': 'assets/jquery.drop.min',
	        'formautofill': 'assets/jquery.formautofill.min',
	        'echarts': 'assets/echarts/echarts.min',
	        'echarts-theme': 'assets/echarts/echarts-theme',
	        'fastclick': 'assets/fastclick/lib/fastclick',
	        'jvectormap': 'assets/jvectormap/jquery-jvectormap-1.2.2.min',
	        'jvectormap-world-mill': 'assets/jvectormap/jquery-jvectormap-world-mill-en',
	        'adminlte': 'assets/AdminLTE/js/app',
	        'dashboard2': 'assets/AdminLTE/js/pages/dashboard2',
	        'jquery': 'assets/jquery/dist/jquery.min',
	        'bootstrap': 'assets/bootstrap/dist/js/bootstrap.min',
	        'bootstrap-datetimepicker': 'assets/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-CN',
	        'bootstrap-select': 'assets/bootstrap-select/dist/js/bootstrap-select.min',
	        'bootstrap-select-lang': 'assets/bootstrap-select/dist/js/i18n/defaults-zh_CN',
	        'bootstrap-table': 'assets/bootstrap-table/dist/bootstrap-table.min',
	        'bootstrap-table-export': 'assets/bootstrap-table/dist/extensions/export/bootstrap-table-export.min',
	        'bootstrap-table-mobile': 'assets/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile',
	        'bootstrap-table-lang': 'assets/bootstrap-table/dist/locale/bootstrap-table-zh-CN',
	        'bootstrap-table-editable':'assets/bootstrap-table/dist/extensions/editable/bootstrap-table-editable',
	        'bootstrap-table-editable-row':'assets/bootstrap-table/dist/extensions/click-edit-row/bootstrap-table-click-edit-row',
	        'bootstrap-switch': 'assets/bootstrap-switch/js/bootstrap-switch.min',
	        'tableexport': 'assets/tableExport.jquery.plugin/tableExport.min',
	        'bootstrap-table-ext': 'assets/bootstrap-table/dist/extensions/bootstrap-table-ext',
	        'dragsort': 'assets/dragsort/jquery.dragsort',
	        'qrcode': 'assets/jquery-qrcode/jquery.qrcode.min',
	        'sortable': 'assets/Sortable/Sortable.min',
	        'addtabs': 'assets/jquery-addtabs/jquery.addtabs',
	        'slimscroll': 'assets/jquery-slimscroll/jquery.slimscroll.min',
	        'sparkline': 'assets/jquery-sparkline/dist/jquery.sparkline.min',
	        'summernote': 'assets/summernote/dist/lang/summernote-zh-CN.min',
	        'validator-core': 'assets/nice-validator/dist/jquery.validator.min',
	        'validator-lang': 'assets/nice-validator/dist/local/zh-CN',
	        'plupload': 'assets/plupload/js/plupload.min',
	        'toastr': 'assets/toastr/toastr',
	        'jstree': 'assets/jstree/dist/jstree.min',
	        'layer': 'assets/layer/src/layer',
	        'cookie': 'assets/jquery.cookie/jquery.cookie',
	        'cxselect': 'assets/jquery-cxselect/js/jquery.cxselect',
	        'template': 'assets/art-template/dist/template-native',
	        'selectpage': 'assets/selectpage/selectpage',
	        'ztree': 'assets/ztree/js/jquery.ztree.all-3.5.min',
	        'treegrid': 'assets/treegrid/js/jquery.treegrid',
	        'treetable': 'assets/jquery-treetable/jquery.treetable',
	        'sitelogo': 'assets/sitelogo/sitelogo',
	        'cropper': 'assets/cropper/cropper.min', 
	        'md5': 'assets/md5', 
	    },
	    // shim依赖配置
	    shim: {
	        'bootstrap': ['jquery'],
	        'form': ['formautofill'],
	        'bootstrap-table': {
	            deps: [
	                'bootstrap', 'css!assets/bootstrap-table/dist/bootstrap-table.min.css',
	            ],
	            exports: '$.fn.bootstrapTable'
	        },
	        'bootstrap-table-lang': {
	            deps: ['bootstrap-table'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-export': {
	            deps: ['bootstrap-table', 'tableexport'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-mobile': {
	            deps: ['bootstrap-table'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-advancedsearch': {
	            deps: ['bootstrap-table'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-search': {
	            deps: ['bootstrap-table'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-template': {
	            deps: ['bootstrap-table', 'template'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-ext': {
	            deps: ['bootstrap-table'],
	        },
	        'bootstrap-table-editable': {
	            deps: ['bootstrap-table',
	            	'css!assets/bootstrap-editable/dist/bootstrap3-editable/css/bootstrap-editable.css',
	            	'assets/bootstrap-editable/dist/bootstrap3-editable/js/bootstrap-editable.min'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-table-editable-row': {
	            deps: ['bootstrap-table', 'css!assets/bootstrap-table/dist/extensions/click-edit-row/bootstrap-table-click-edit-row.css'],
	            exports: '$.fn.bootstrapTable.defaults'
	        },
	        'bootstrap-switch': ['css!assets/bootstrap-switch/css/bootstrap3/bootstrap-switch.min.css'],
	        'tableexport': {
	            deps: ['jquery'],
	            exports: '$.fn.extend'
	        },
	        'slimscroll': {
	            deps: ['jquery'],
	            exports: '$.fn.extend'
	        },
	        'adminlte': {
	            deps: ['bootstrap', 'fastclick', 'sparkline', 'slimscroll', 'assets/AdminLTE/js/app_iframe'],
	            exports: '$.AdminLTE'
	        },
	        'layer': ['css!assets/layer/build/theme/default/layer.css'],
	        'bootstrap-datetimepicker': [ 'css!assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css', 
	        	'assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min', ],
	        'bootstrap-select': ['css!assets/bootstrap-select/dist/css/bootstrap-select.min.css', ],
	        'bootstrap-select-lang': ['bootstrap-select'],
	        'summernote': ['assets/summernote/dist/summernote.min', 'css!assets/summernote/dist/summernote.css'],
	        'jstree': ['css!assets/jstree/dist/themes/default/style.css', ],
	        'plupload': {
	            deps: ['assets/plupload/js/moxie.min'],
	            exports: "plupload"
	        },
	        'validator-lang': ['validator-core', 'css!assets/nice-validator/dist/jquery.validator.css'],
	        'ztree': ['css!assets/ztree/css/zTreeStyle.css'],
	        'treegrid': ['css!assets/treegrid/css/jquery.treegrid.css'],
	        'treetable': ['css!assets/jquery-treetable/css/jquery.treetable.theme.default.css'],
	        'selectpage': ['css!assets/selectpage/selectpage.css'],
	        'sitelogo': ['css!assets/sitelogo/sitelogo.css'],
	        'cropper': ['css!assets/cropper/cropper.min.css']
	    },
	    baseUrl: requirejs.s.contexts._.config.config.site.cdnurl, //资源基础路径
	    map: {
	        '*': {
	            'css': 'assets/require-css/css.min'
	        }
	    },
	    waitSeconds: 30,
	    charset: 'utf-8' // 文件编码
	});
});