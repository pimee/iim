define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'ztree'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/role/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/role/read/page',
                    add_url: 'roleEdit.html',
                    edit_url: 'roleEdit.html',
                    del_url: '/role',
                    multi_url: '/role',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'roleName', title: '角色名称'},
                        /*{field: 'orgaName', title: '所属组织'},*/
                        {field: 'remark', title: '备注'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.userType == 3) {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        	Util.api.initSelectTree({
            	checkParent: true,
            	parentEle: 'organization',
        		url: '/organization/read/list',
        		data: JSON.stringify({keyword:''}),
        		resetData: function(data) {
        		   for(var i=0;i<data.length;i++) {
        			   data[i]['open'] = true;
        			   data[i]['pId'] = data[i].parentId;
        			   data[i]['name'] = data[i].orgaName;
        		   }
        		   return data;
            	}
            });
        	var id = Util.api.query('id');
        	var t = Util.api.query('t');
        	if(id && t === 'U') {
        		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
        		});
        	}
        }
    };
    return Controller;
});