define(['jquery', 'bootstrap', 'utilback', 'form', 'ztree', 'selectpage'], function ($, undefined, Utilback, Form) {

    var Controller = {
    	config: {
    		get_url: '/organization/read/detail',
    	},
        init: function () {
	    	var ms = $('#managerId').selectPage({
	    		instance: true,
        			data: '/user/read/page',
        			showField: 'userName',
	    		    keyField : 'id',
        		    searchField: 'keyword',
	    			eAjaxSuccess: function(data) {
	    				return {
	    					"pageSize": data.size,
	    		            "pageNumber": data.current,
	    		            "totalRow": data.total,
	    		            "totalPage": data.pages,
	    		            "list": data.rows
	    				};
	    			}
	    		});
            
        	$('#edit-form button').hide();
        	$('.form-group.hide').removeClass('hide');
        	var treeObj;
            //新增职位
            function addHoverDom(treeId, treeNode) {
                var sObj = $("#" + treeNode.tId + "_span"); //获取节点信息
                if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;

                var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' data-id='"
                	+ treeNode.id + "' data-name='" + treeNode.orgaName
                	+ "' title='新增' onfocus='this.blur();'></span>"; //定义添加按钮
                sObj.after(addStr); //加载添加按钮
                var btn = $("#addBtn_" + treeNode.tId);

                //添加部门
                if (btn) btn.bind("click", function (event) {
                    var zTree = $.fn.zTree.getZTreeObj("tree");
                    $('#edit-form input,#edit-form select,#edit-form textarea').removeAttr('disabled').val('');
                    $('#edit-form input[name="parentId"]').val($(this).attr('data-id'));
                    $('#edit-form input[name="parentName"]').val($(this).attr('data-name'));
                	$('#edit-form button').show();
                    event.stopPropagation();
                });
            };

            function removeHoverDom(treeId, treeNode) {
            	$("#addBtn_" + treeNode.tId).remove();
            };

            //点击部门显示对应信息
            function zTreeOnClick(event, treeId, treeNode) {
            	$('#edit-form button').hide();
                $('#edit-form').autofill(treeNode);
                if(treeNode.managerId) {
            		$('#managerId').val(treeNode.managerId);
            		$('#managerId').selectPageRefresh();
                } else {
                	$('#managerId').selectPageClear();
                }
                $('#edit-form input,#edit-form select,#edit-form textarea').attr("disabled", 'disabled');
            }

            //编辑部门信息
            function editDom(treeId, treeNode) {
            	$('#edit-form button').show();
                $('#edit-form input,#edit-form select,#edit-form textarea').removeAttr('disabled');
                $('#edit-form').autofill(treeNode);
                if(treeNode.managerId) {
            		$('#managerId').val(treeNode.managerId);
            		$('#managerId').selectPageRefresh();
                } else {
                	$('#managerId').selectPageClear();
                }
                return false;
            }
            function deleteDom(treeId, treeNode) {
            	Util.api.ajax({
             	   type: 'DELETE',
             	   data: JSON.stringify({id: treeNode.id}),
             	   url: '/organization',
                 }, function() {
                 	treeObj.removeNode(treeNode);
                 	return true;
                 });
            	return false;
            }
        	/**
             * 部门树结构
             */
            var setting = {
                view: {
                    addHoverDom: addHoverDom, //当鼠标移动到节点上时，显示用户自定义控件
                    removeHoverDom: removeHoverDom//离开节点时的操作
                },
                check: {
                    enable: false,
                },
                data: {
                    simpleData: {
                        enable: true
                    }
                },
                edit: {
                    enable: true
                },
                treeNode: {
                    checked: false
                },
                callback: {
                    onClick: zTreeOnClick, //单击事件
                    beforeEditName: editDom, //编辑
                    beforeRemove: deleteDom
                }
            };
            function initDeptTree() {
                Util.api.ajax({
            	   type: 'PUT',
            	   data: JSON.stringify({keyword:''}),
            	   url: '/organization/read/list',
                }, function(result) {
        			   var data = result.rows;
        			   for(var i=0;i<data.length;i++) {
        				   data[i]['open'] = true;
        				   data[i]['pId'] = data[i].parentId;
        				   data[i]['name'] = data[i].orgaName;
        			   }
        			   treeObj = $.fn.zTree.init($("#departmentTree"), setting, data);
        		       
    				   Util.api.initSelectTree({
    		            	checkParent: true,
    		            	parentEle: 'deptEdit',
    		        		data: data
    		        	});
             	});
            }
		    initDeptTree();
		    $('#toolbar').on('click', '.btn-refresh', function() {
		    	initDeptTree();
		    });
            Form.api.bindevent($("form[role=form]"), function(){
				return true;
            }, function() {
            	setTimeout(initDeptTree, 1000);
            });
        }
    };
    return Controller;
});