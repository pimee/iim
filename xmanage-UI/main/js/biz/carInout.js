define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/carInout/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	search: false,
            	showExport: false,
            	singleSelect: true,
            	showToggle: false,
            	showColumns: false,
            	commonSearch: false,
                extend: {
                    index_url: '/carInout/read/list',
                    del_url: '/carInout',
                    multi_url: '/carInout',
                }
            });

        	var id = Util.api.query('id');
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                queryParams: function(param) {
                	param.carId = id;
                	return param;
                },
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'inOutTime', title: __('出入时间')},
                        {field: 'inOut', title: __('类型')},
                        {field: 'money', title: __('金额')},
                        {field: 'inOutDoor', title: __('出入口')},
                        {field: 'orderNo', title: __('订单号')},
                        {field: 'payWay', title: __('缴费方式')},
                        {field: 'parkModel', title: __('停车模式')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});