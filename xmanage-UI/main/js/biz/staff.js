define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'ztree', 'md5'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/staff/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/staff/read/list',
                    add_url: 'staffEdit.html',
                    edit_url: 'staffEdit.html',
                    del_url: '/staff',
                    multi_url: '/staff',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'staffNo', title: '工号'},
                        {field: 'staffName', title: __('Username')},
                        {field: 'staffPhone', title: __('Mobile')},
                        {field: 'sex', title: '性别', formatter: function (v, row, index) {
                        	return v == '1' ? '男' : v == '2' ? '女' : '保密';
                        }},
                        {field: 'avatar', title: '头像', formatter: function (v, row, index) {
                        	return v ? '<img class="view" title="' + row.userName + '" height="30px" src="'+v+'">' : '';
                        }},
                        {field: 'orgaName', title: __('所属组织')},
                        {field: 'isOnline', title: __("Status"), formatter: function (v, row, index) {
                        	return v == '1' ? '<span class="label label-info">在线</span>' : '<span class="label label-default">下线</span>';
                        }},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.userType == 3) {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            Util.api.initSelectTree({
            	checkParent: true,
            	parentEle: 'organization',
        		url: '/organization/read/list',
        		data: JSON.stringify({keyword:''}),
        		resetData: function(data) {
        		   for(var i=0;i<data.length;i++) {
        			   data[i]['open'] = true;
        			   data[i]['pId'] = data[i].parentId;
        			   data[i]['name'] = data[i].orgaName;
        		   }

           		   var id = Util.api.query('id');
                   if(id) {
		           		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
		       				if(data.avatar) {
		       					$('#avatar').append('<img height="30px" src="'+data.avatar+'">');
		       				}
		           		});
                   }
        		   return data;
            	}
            });
        }
    };
    return Controller;
});