define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/appVersion/read/detail',
    	},
        init: function () {
        	var platforms = {};
        	Util.api.ajax({
        		type: 'PUT',
        		url: '/dic/read/list',
        		data: JSON.stringify({type: 'PLATFORMTYPE'}),
        	}, function(rst) {
        		$.each(rst.rows, function(i, v) {
        			platforms[v.code] = v.codeText;
        		});
        		
        		// 初始化表格参数配置
                Table.api.init({
                	singleSelect: true,
                	detailView: true,
                    extend: {
                        index_url: '/appVersion/read/list',
                        add_url: 'appVersionEdit.html',
                        edit_url: 'appVersionEdit.html',
                        del_url: '/appVersion',
                        multi_url: '/appVersion',
                    }
                });

                var table = $("#table");

                // 初始化表格
                table.bootstrapTable({
                    url: $.fn.bootstrapTable.defaults.extend.index_url,
                    columns: [
                        [
                            {field: 'state', checkbox: true, },
                            {field: 'id', title: 'ID'},
                            {field: 'platform', title: '平台类型', formatter: function (value, row, index) {
                                return platforms[value];
                            }},
                            {field: 'version', title: '版本号'},
                            {field: 'url', title: '安装包地址'},
                            {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                             }}
                        ]
                    ],
                    detailFormatter: function(index, row) {
                    	return row.activityContent;
                    }
                });

                // 为表格绑定事件
                Table.api.bindevent(table);
        	});
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"), function() {
            });
        	Util.api.initSelectOption({
            	target: '[name="platform"]',
            	param: {type: 'PLATFORMTYPE'},
            	init: function() {
            		var id = Util.api.query('id');
                	var t = Util.api.query('t');
                	if(id && t === 'U') {
                		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
                		});
                	}
            	}
            });
        }
    };
    return Controller;
});