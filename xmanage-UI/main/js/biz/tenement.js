define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/tenement/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/tenement/read/list',
                    add_url: 'tenementEdit.html',
                    edit_url: 'tenementEdit.html',
                    del_url: '/tenement',
                    multi_url: '/tenement',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'fullName', title: __('姓名')},
                        {field: 'phone', title: __('联系方式')},
                        {field: 'houseName', title: __('所属房屋')},
                        {field: 'mentType', title: __('类型'), formatter: function (value, row, index) {
                        	return value==1 ? '业主' : value==2 ? '住户' : value==3 ? '租客' : '-';
                        }},
                        {field: 'carQuantity', title: __('车辆数')},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                             return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var iniData;
            Util.api.initSelectOption({
    			url: '/area/read/list',
            	target: '[name="areaId"]',
            	param: { orderBy: 'area_name', sortAsc: 'Y' },
            	codeText: 'areaName',
    		    code : 'id',
    		    init: function() {
            		var id = Util.api.query('id');
                	if(id) {
        	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
        	        		iniData = data;
        	        		$('[name="areaId"]').selectpicker('val', data.areaId);
        	        		$('[name="areaId"]').change();
        	        	});
                	}
            	}
            });
    		$('[name="areaId"]').on('change', function(){
    			if(!$(this).val()) return;
            	Util.api.initSelectOption({
        			url: '/building/read/list',
                	target: '[name="buildingId"]',
                	param: { areaId: $('[name="areaId"]').val(), orderBy: 'building_name', sortAsc: 'Y' },
                	codeText: 'buildingName',
        		    code : 'id',
        		    init: function() {
                		if(iniData) {
        	        		$('[name="buildingId"]').selectpicker('val', iniData.buildingId);
        	        		$('[name="buildingId"]').change();
                		}
                	}
            	});
            });

    		$('[name="buildingId"]').on('change', function(){
    			if(!$(this).val()) return;
                Util.api.initSelectOption({
        			url: '/house/read/list',
                	target: '[name="houseId"]',
                	param: { buildingId: $('[name="buildingId"]').val(), orderBy: 'house_no', sortAsc: 'Y' },
                	codeText: 'houseNo',
        		    code : 'id',
        		    init: function() {
                		if(iniData) {
        	        		$('[name="houseId"]').selectpicker('val', iniData.houseId);
                		}
                	}
                });
    		});
        }
    };
    return Controller;
});