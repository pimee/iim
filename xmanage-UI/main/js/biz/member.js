define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/member/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/member/read/list',
                    add_url: 'memberEdit.html',
                    edit_url: 'memberEdit.html',
                    del_url: '/member',
                    multi_url: '/member',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'userName', title: __('Username')},
                        {field: 'phone', title: __('Mobile')},
                        {field: 'nickName', title: '昵称'},
                        {field: 'sex', title: '性别', formatter: function (v, row, index) {
                        	return v == '1' ? '男' : v == '2' ? '女' : '保密';
                        }},
                        {field: 'qrCode', title: '二维码', formatter: function (v, row, index) {
                        	return v ? '<img class="view" title="' + row.userName + '" height="30px" src="'+v+'">' : '';
                        }},
                        {field: 'source', title: '来源'},
                        {field: 'avatar', title: '头像', formatter: function (v, row, index) {
                        	return v ? '<img class="view" title="' + row.userName + '" height="30px" src="'+v+'">' : '';
                        }},
                        {field: 'isOnline', title: __("Status"), formatter: function (v, row, index) {
                        	return v == '1' ? '<span class="label label-info">在线</span>' : '<span class="label label-default">下线</span>';
                        }},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.userType == 3) {
                                    return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        	Util.api.initSelectOption({
            	target: '[name="locationCountry"]',
            	param: {type: 'COUNTRY'},
            	init: function() {
            		var id = Util.api.query('id');
                	var t = Util.api.query('t');
                	if(id && t === 'U') {
                		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
            				if(data.avatar) {
            					$('#avatar').append('<img height="30px" src="'+data.avatar+'">');
            				}
            				if(data.qrCode) {
            					$('#qrCode').append('<img height="30px" src="'+data.qrCode+'">');
            				}if($('[name="sex"][value="' + data.sex + '"]').length > 0) {
                    			$('[name="sex"][value="' + data.sex + '"]').get(0).checked = true;
                    			$('[name="sex"][value="' + data.sex + '"]').parents('span').addClass('checked');
                			}
            				$('[name="locationCountry"]').selectpicker('val',data.locationCountry);
                			$.each(['Province', 'City', 'Area'], function(i, v) {
            		    		if(data['location' + v + 's']) {
            	                    $('[name="location'+v+'"]').append('<option value="">-请选择-</option>');
            		    			$.each(result['location' + v + 's'], function(n, value) {
            		    				if(data['location' + v] == value.code) {
                	                        $('[name="location'+v+'"]').append('<option value="' + value.code + '" selected>' + value.codeText + '</option>');
            		    				} else {
            		    					$('[name="location'+v+'"]').append('<option value="' + value.code + '">' + value.codeText + '</option>');
            		    				}
            	 					});
            		    		}
                			});
                			$('[name="enable"]').bootstrapSwitch('state', data.enable == '1');
                		});
                	}
            	}
            });
        	
            $('[name="locationCountry"]').on('change', function() {
            	if($(this).val().length > 0) {
            		Util.api.initSelectOption({
            			target: '[name="locationProvince"]',
            			param: { type:'PROVINCE', parentType: 'COUNTRY', parentCode:$(this).val()}
            		});
            	} else {
            		$.each(['Province', 'City', 'Area'], function(m, t) {
         				$('[name="location' + t + '"]').html('');
            		});
            	}
            });
            $('[name="locationProvince"]').on('change', function() {
            	if($(this).val().length > 0) {
            		Util.api.initSelectOption({
                    	target: '[name="locationCity"]',
                    	param: { type:'CITY', parentType: 'PROVINCE', parentCode:$(this).val()}
                	});
            	} else {
            		$.each(['City', 'Area'], function(m, t) {
         				$('[name="location' + t + '"]').html('');
            		});
            	}
            });
            $('[name="locationCity"]').on('change', function() {
            	if($(this).val().length > 0) {
            		Util.api.initSelectOption({
                    	target: '[name="locationArea"]',
                    	param: { type:'AREA', parentType: 'CITY', parentCode:$(this).val()}
                	});
            	} else {
         			$('[name="locationArea"]').html('');
            	}
            });
        }
    };
    return Controller;
});