define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'upload', 'selectpage'], function ($, undefined, Utilback, Table, Form, Upload) {

    var Controller = {
    	config: {
    		get_url: '/house/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/house/read/list',
                    add_url: 'houseEdit.html',
                    edit_url: 'houseEdit.html',
                    del_url: '/house',
                    multi_url: '/house',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                exportOptions:{
                    ignoreColumn: [0,1,8,9,10,11],  //忽略某一列的索引
                },
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'areaName', title: __('所属区域')},
                        {field: 'buildingName', title: __('所属楼栋')},
                        {field: 'houseNo', title: __('房屋编号')},
                        {field: 'ownerName', title: __('业主姓名')},
                        {field: 'ownerCard', title: __('业主身份证号')},
                        {field: 'ownerPhone', title: __('业主手机号')},
                        {field: 'manageFee', title: '物业费(元/月)'},
                        {field: 'houseStatus', title: __('房屋状态'), formatter: function (value, row, index) {
                        	return value==1 ? '空置' : value==2 ? '装修中' : value==3 ? '自住' : value==4 ? '出租' : '-';
                        }},
                        {field: 'houseType', title: __('产权类型'), formatter: function (value, row, index) {
                        	return value==1 ? '住宅' : value==2 ? '别墅' : value==3 ? '写字楼' : value==4 ? '商铺' : value==5 ? '自住型商品房' : '-';
                        }},
                        {field: 'ownStatus', title: __('产证状态'), formatter: function (value, row, index) {
                        	return value==0 ? '未办理' : value==1 ? '已办理' : '-';
                        }},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                             return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            
            Upload.api.plupload();
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
    		for(var i=1; i<10; i++) {
    			$('[name="bedroom"],[name="livingroom"],[name="toilet"]').append('<option value="'+i+'">'+i+'</option>');
    		}
            var iniData;
    		Util.api.initSelectOption({
    			url: '/area/read/list',
            	target: '[name="areaId"]',
            	param: { orderBy: 'area_name', sortAsc: 'Y' },
            	codeText: 'areaName',
    		    code : 'id',
            	init: function() {
		            var id = Util.api.query('id');
		        	if(id) {
			        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
        	        		iniData = data;
			        		$('[name="managerId"]').attr('data-init', data.managerId);
			        		initManagerId();
        	        		$('[name="areaId"]').selectpicker('val', data.areaId);
			        		$('[name="areaId"]').change();
			        	});
		        	} else {
		        		initManagerId();
		        	}

		        	function initManagerId() {
			    		$('[name="managerId"]').selectPage({
	            			data: '/user/read/page',
	            			showField: 'userName',
			    		    keyField : 'id',
	            		    searchField: 'keyword',
			    			eAjaxSuccess: function(data) {
			    				return {
			    					"pageSize": data.size,
			    		            "pageNumber": data.current,
			    		            "totalRow": data.total,
			    		            "totalPage": data.pages,
			    		            "list": data.rows
			    				};
			    			}
			    		});
		        	}
            	}
    		});
    		$('[name="areaId"]').on('change', function(){
    			if(!$(this).val()) return;
            	Util.api.initSelectOption({
        			url: '/building/read/list',
                	target: '[name="buildingId"]',
                	param: { areaId: $('[name="areaId"]').val(), orderBy: 'building_name', sortAsc: 'Y' },
                	codeText: 'buildingName',
        		    code : 'id',
        		    init: function() {
                		if(iniData) {
        	        		$('[name="buildingId"]').selectpicker('val', iniData.buildingId);
                		}
                	}
            	});
            });
        }
    };
    return Controller;
});