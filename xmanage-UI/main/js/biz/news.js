define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/news/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/news/read/list',
                    add_url: 'newsEdit.html',
                    edit_url: 'newsEdit.html',
                    del_url: '/news',
                    multi_url: '/news',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'newsTitle', title: __('标题')},
                        {field: 'newsTypeName', title: __('栏目')},
                        {field: 'sendTime', title: __('发布时间')},
                        {field: 'readerTimes', title: __('阅读次数')},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                             return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            Util.api.initSelectOption({
            	target: '[name="newsType"]',
            	param: { type: 'NEWSTYPE' },
            	init: function() {
                    var id = Util.api.query('id');
                	if(id) {
        	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
        	        		$('[name="newsType"]').selectpicker('val', data.newsType);
        	        	});
                	}
            	}
            });
        }
    };
    return Controller;
});