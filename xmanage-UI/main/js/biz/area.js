define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/area/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/area/read/page',
                    add_url: 'areaEdit.html',
                    edit_url: 'areaEdit.html',
                    del_url: '/area',
                    multi_url: '/area',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'areaName', title: __('小区名称')},
                        {field: 'manageFee', title: '物业费(元/月/平方)'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                             return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});