define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/patrol/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	showExport: false,
            	singleSelect: true,
            	showToggle: false,
            	showColumns: false,
            	commonSearch: false,
                extend: {
                    index_url: '/attendance/read/patrolList',
                    del_url: '/patrol',
                    multi_url: '/patrol',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'staffNo', title: __('工号')},
                        {field: 'staffName', title: __('姓名')},
                        {field: 'areaName', title: __('所属区域')},
                        {field: 'signTime1', title: __('上班时间')},
                        {field: 'signTime2', title: __('下班时间')},
                        {field: 'operate', title: __('Operate'), events: {
                        		'click .btn-dispose': function(e, value, row, index) {
                                	e.stopPropagation();
                                    var id = row.id;
                                    
                                }
	                        }, formatter: function (value, row, index) {
	                            return '<a href="javascript:;" class="btn btn-info btn-dispose btn-xs"><i class="fa fa-cog"></i></a>';
	                        }
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});