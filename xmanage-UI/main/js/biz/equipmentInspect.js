define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/equipmentInspect/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	search: false,
            	showExport: false,
            	singleSelect: true,
            	showToggle: false,
            	showColumns: false,
            	commonSearch: false,
                extend: {
                    index_url: '/equipmentInspect/read/list',
                    del_url: '/equipmentInspect',
                    multi_url: '/equipmentInspect',
                }
            });

        	var id = Util.api.query('id');
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                queryParams: function(param) {
                	param.equipmentId = id;
                	return param;
                },
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'inspectTime', title: __('巡检时间')},
                        {field: 'inspector', title: __('巡检人')},
                        {field: 'equipmentStatus', title: __('状态')},
                        {field: 'remark', title: __('备注')},
                        {field: 'workOrder', title: __('工单号')},
                        {field: 'orderStatus', title: __('工单状态')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});