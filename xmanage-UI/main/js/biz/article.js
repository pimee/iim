define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/article/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/article/read/list',
                    add_url: 'articleEdit.html',
                    edit_url: 'articleEdit.html',
                    del_url: '/article',
                    multi_url: '/article',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'title', title: __('文章标题')},
                        {field: 'typeName', title: __('文章类别')},
                        {field: 'createTime', title: __('添加时间')},
                        {field: 'enable', title: __('状态'), formatter: function(v, row, index) {
                       		return v=='1' ? '<span class="label label-info">显示</span>' : '<span class="label label-default">隐藏</span>';
                       	}},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                             return Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            Util.api.initSelectOption({
            	target: '[name="type"]',
            	param: { type: 'ARTICLETYPE' },
            	init: function() {
                    var id = Util.api.query('id');
                	if(id) {
        	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
        	        		$('[name="type"]').selectpicker('val', data.type);
        	        	});
                	}
            	}
            });
        }
    };
    return Controller;
});