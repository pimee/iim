define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    	},
        init: function () {
        	var map = new AMap.Map("mapContainer", {
                resizeEnable: true,
                zoom: 13
            });
        	$('#mapContainer').height($(window).height() - 105);
        	$(window).on('resize', function() {
        		$('#mapContainer').height($(window).height() - 105);
        	});
        	
            var marker = new AMap.Marker({
            	map: map,
            	content: '<div class="markerContainer"><div class="marker">维埃拉-保安-139123456789</div><img height="25px" src="../../../extends/img/mark_r.png"/></div>',
                position: [113.505467, 34.807761],
                offset: new AMap.Pixel(-50, -57)
            });
            map.setFitView();
        },
    };
    return Controller;
});