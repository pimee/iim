define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'selectpage'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/car/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/car/read/list',
                    add_url: 'carEdit.html',
                    edit_url: 'carEdit.html',
                    del_url: '/car',
                    multi_url: '/car',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'plateNumber', title: __('车牌')},
                        {field: 'carType', title: __('汽车型号')},
                        {field: 'linkMan', title: __('联系人')},
                        {field: 'houseName', title: __('所属房屋')},
                        {field: 'carportType', title: __('类型'), formatter: function (value, row, index) {
                        	return value==1 ? '车位车辆' : value==2 ? '月租车辆' : value==3 ? '计时车辆' : value==4 ? '免费车辆' : '-';
                        }},
                        {field: 'carportNo', title: __('车位编号')},
                        {field: 'carStatus', title: __('状态'), formatter: function (value, row, index) {
                        	return value==1 ? '正常' : value==2 ? '异常' : '-';
                        }},
                        {field: 'operate', title: __('Operate'), events: $.extend({'click .btn-fee': function (e, value, row, index) {
                        	e.stopPropagation();
                            var options = $(this).closest('table').bootstrapTable('getOptions');
                            Util.api.open('carPayment.html?id=' + row[options.pk], row.plateNumber+ '-缴费记录');
                        }, 'click .btn-io': function(e, value, row, index) {
                        	e.stopPropagation();
                            var options = $(this).closest('table').bootstrapTable('getOptions');
                            Util.api.open('carInout.html?id=' + row[options.pk], row.plateNumber+ '-出入记录');
                        }}, Table.api.events.operate), formatter: function (value, row, index) {
                            var btn = '<a href="javascript:;" class="btn btn-info btn-io btn-xs"><i class="fa fa-arrows-h"></i></a> '
                            	+ '<a href="javascript:;" class="btn btn-warning btn-fee btn-xs"><i class="fa fa-rmb"></i></a> '; 
                        	return btn + Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        		$('[name="houseId"]').attr('data-init', data.houseId);
	        		init();
	        	});
        	} else {
        		init();
        	}
        	
        	function init() {
        		$('[name="houseId"]').selectPage({
        			data: '/house/read/page',
        			showField: 'houseNo',
        		    keyField : 'id',
        		    searchField: 'keyword',
        			eAjaxSuccess: function(data) {
        				return {
        					"pageSize": data.size,
        		            "pageNumber": data.current,
        		            "totalRow": data.total,
        		            "totalPage": data.pages,
        		            "list": data.records
        				};
        			}
        		});
        	}
        }
    };
    return Controller;
});