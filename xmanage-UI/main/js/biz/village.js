define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'selectpage'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/village/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/village/read/list',
                    add_url: 'villageEdit.html',
                    edit_url: 'villageEdit.html',
                    del_url: '/village',
                    multi_url: '/village',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'villageName', title: __('小区名称')},
                        {field: 'location', title: __('地址'), formatter: function (value, row, index) {
                        	return value ? value + row.locationDetail : value;
                        }},
                        {field: 'buildTime', title: '建成年份'},
                        {field: 'orgaName', title: __('公司')},
                        {field: 'managerNames', title: '管理员'},
                        {field: 'operate', title: __('Operate'), events: Table.api.events.operate, formatter: function (value, row, index) {
                                return Table.api.formatter.operate.call(this, value, row, index, table);
                         }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
        	Util.api.initSelectOption({
            	target: '[name="locationProvince"]',
            	param: {type: 'PROVINCE', parentType: 'COUNTRY', parentCode: 'ZHCN'},
            	init: function() {
            		var id = Util.api.query('id');
                	var t = Util.api.query('t');
                	if(id && t === 'U') {
                		Form.api.fill($('#edit-form'), Controller.config.get_url, id, function(data) {
			        		$('[name="managerId"]').attr('data-init', data.managerId);
			        		initManagerId();
            				$('[name="locationProvince"]').selectpicker('val',data.locationProvince);
                			$.each(['City', 'Area'], function(i, v) {
            		    		if(data['location' + v + 's']) {
            	                    $('[name="location'+v+'"]').append('<option value="">-请选择-</option>');
            		    			$.each(data['location' + v + 's'], function(n, value) {
            		    				if(data['location' + v] == value.code) {
                	                        $('[name="location'+v+'"]').append('<option value="' + value.code + '" selected>' + value.codeText + '</option>');
            		    				} else {
            		    					$('[name="location'+v+'"]').append('<option value="' + value.code + '">' + value.codeText + '</option>');
            		    				}
            	 					});
            		    		}
                			});
                		});
                	} else {
                		initManagerId();
                	}

                    $('[name="locationProvince"]').on('change', function() {
                    	if($(this).val().length > 0) {
                    		Util.api.initSelectOption({
                            	target: '[name="locationCity"]',
                            	param: { type:'CITY', parentType: 'PROVINCE', parentCode:$(this).val()}
                        	});
                    	} else {
                    		$.each(['City', 'Area'], function(m, t) {
                 				$('[name="location' + t + '"]').html('');
                    		});
                    	}
                    });
                    $('[name="locationCity"]').on('change', function() {
                    	if($(this).val().length > 0) {
                    		Util.api.initSelectOption({
                            	target: '[name="locationArea"]',
                            	param: { type:'AREA', parentType: 'CITY', parentCode:$(this).val()}
                        	});
                    	} else {
                 			$('[name="locationArea"]').html('');
                    	}
                    });
                    
                    function initManagerId() {
			    		$('[name="managerId"]').selectPage({
	            			data: '/user/read/page',
	            			showField: 'userName',
			    		    keyField : 'id',
	            		    searchField: 'keyword',
			    			eAjaxSuccess: function(data) {
			    				return {
			    					"pageSize": data.size,
			    		            "pageNumber": data.current,
			    		            "totalRow": data.total,
			    		            "totalPage": data.pages,
			    		            "list": data.rows
			    				};
			    			}
			    		});
		        	}
            	}
            });
        }
    };
    return Controller;
});