define(['jquery', 'bootstrap', 'utilback', 'table', 'form', 'selectpage'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/equipment/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	singleSelect: true,
                extend: {
                    index_url: '/equipment/read/list',
                    add_url: 'equipmentEdit.html',
                    edit_url: 'equipmentEdit.html',
                    del_url: '/equipment',
                    multi_url: '/equipment',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'state', checkbox: true, },
                        {field: 'id', title: 'ID'},
                        {field: 'name', title: __('设备名称')},
                        {field: 'no', title: __('设备编号')},
                        {field: 'areaName', title: __('所属区域')},
                        {field: 'buildingName', title: __('所属楼栋')},
                        {field: 'type', title: __('设备类型'), formatter: function (value, row, index) {
                        	return value==1 ? '消防设备' : value==2 ? '别墅' : value==3 ? '写字楼' : value==4 ? '商铺' : value==5 ? '自住型商品房' : '-';
                        }},
                        {field: 'status', title: __('设备状态'), formatter: function (value, row, index) {
                        	return value==1 ? '正常' : value==2 ? '异常' : '-';
                        }},
                        {field: 'inspectorId', title: __('设备维护人'), formatter: function (value, row, index) {
                        	return row.inspectorName + '/' + row.inspectorPhone;
                        }},
                        {field: 'lastInspectTime', title: __('最后巡检时间')},
                        {field: 'location', title: __('存放位置')},
                        {field: 'operate', title: __('Operate'), events: $.extend({'click .btn-inspect': function (e, value, row, index) {
                        	e.stopPropagation();
                            var options = $(this).closest('table').bootstrapTable('getOptions');
                            Util.api.open('equipmentInspect.html?id=' + row[options.pk], row.areaName + '-'
                            		+ row.buildingName + '-' + row.name + '-巡检记录');
                        }}, Table.api.events.operate), formatter: function (value, row, index) {
                            var btn = '<a href="javascript:;" class="btn btn-info btn-inspect btn-xs"><i class="fa fa-eye"></i></a> '
                             return btn + Table.api.formatter.operate.call(this, value, row, index, table);
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var iniData;
            Util.api.initSelectOption({
    			url: '/area/read/list',
            	target: '[name="areaId"]',
            	param: { orderBy: 'area_name', sortAsc: 'Y' },
            	codeText: 'areaName',
    		    code : 'id',
            	init: function() {
		            var id = Util.api.query('id');
		        	if(id) {
			        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
        	        		iniData = data;
			        		$('[name="inspectorId"]').val(data.inspectorId);
			        		$('[name="inspectorId"]').attr('data-init', data.inspectorId);
        	        		$('[name="areaId"]').selectpicker('val', data.areaId);
			        		$('[name="areaId"]').change();
			        	});
		        	}

		    		$('[name="inspectorId"]').selectPage({
            			data: '/user/read/page',
            			showField: 'userName',
		    		    keyField : 'id',
            		    searchField: 'keyword',
		    			eAjaxSuccess: function(data) {
		    				return {
		    					"pageSize": data.size,
		    		            "pageNumber": data.current,
		    		            "totalRow": data.total,
		    		            "totalPage": data.pages,
		    		            "list": data.records
		    				};
		    			}
		    		});
            	}
    		});
            
            $('[name="areaId"]').on('change', function(){
            	Util.api.initSelectOption({
        			url: '/building/read/list',
                	target: '[name="buildingId"]',
                	param: { areaId: $('[name="areaId"]').val() },
                	codeText: 'buildingName',
        		    code : 'id',
        		    init: function() {
                		if(iniData) {
        	        		$('[name="buildingId"]').selectpicker('val', iniData.buildingId);
                		}
                	}
            	});
            });
        }
    };
    return Controller;
});