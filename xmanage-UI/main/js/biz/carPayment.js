define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/carPayment/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	search: false,
            	showExport: false,
            	singleSelect: true,
            	showToggle: false,
            	showColumns: false,
            	commonSearch: false,
                extend: {
                    index_url: '/carPayment/read/list',
                    del_url: '/carPayment',
                    multi_url: '/carPayment',
                }
            });

        	var id = Util.api.query('id');
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                queryParams: function(param) {
                	param.carId = id;
                	return param;
                },
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'payTime', title: __('充值时间')},
                        {field: 'money', title: __('金额')},
                        {field: 'payType', title: __('支付方式')},
                        {field: 'remark', title: __('备注')},
                        {field: 'orderNo', title: __('订单号')},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        }
    };
    return Controller;
});