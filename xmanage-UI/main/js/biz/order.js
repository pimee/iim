define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/order/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	search: true,
            	showExport: true,
            	singleSelect: true,
            	showToggle: true,
            	showColumns: true,
            	commonSearch: false,
                extend: {
                    index_url: '/order/read/list',
                }
            });

        	var id = Util.api.query('id');
            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                queryParams: function(param) {
                	param.carId = id;
                	return param;
                },
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'orderDate', title: __('账单年月')},
                        {field: 'bizType', title: __('类型'), formatter: function (value, row, index) {
                        	return value=='01' ? '物业费' : value=='02' ? '维修费' : value=='03' ? '停车费' : value=='04' ? '停车费充值' : '-';
                        }},
                        {field: 'orderMoney', title: __('费用')},
                        {field: 'orderTime', title: __('账单日期')},
                        {field: 'houseNo', title: __('所属房屋')},
                        {field: 'payTime', title: __('缴费日期')},
                        {field: 'operate', title: __('Operate'), events: {'click .btn-fee': function (e, value, row, index) {
                        	e.stopPropagation();
                           
                        }}, formatter: function (value, row, index) {
                            var btn = ' '; 
                        	return btn;
                        }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});