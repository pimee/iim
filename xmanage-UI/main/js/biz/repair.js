define(['jquery', 'bootstrap', 'utilback', 'table', 'form'], function ($, undefined, Utilback, Table, Form) {

    var Controller = {
    	config: {
    		get_url: '/repair/read/detail',
    	},
        init: function () {
            // 初始化表格参数配置
            Table.api.init({
            	search: false,
            	showExport: false,
            	singleSelect: true,
            	showToggle: false,
            	showColumns: false,
            	commonSearch: false,
                extend: {
                    index_url: '/repair/read/list',
                    del_url: '/repair',
                    multi_url: '/repair',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                columns: [
                    [
                        {field: 'id', title: 'ID'},
                        {field: 'workOrder', title: __('工单号')},
                        {field: 'workType', title: __('类型')},
                        {field: 'workDetail', title: __('详情')},
                        {field: 'workFee', title: __('费用')},
                        {field: 'createTime', title: __('下单时间')},
                        {field: 'creator', title: __('创建人')},
                        {field: 'disposer', title: __('处置人')},
                        {field: 'acceptTime', title: __('接受时间')},
                        {field: 'operate', title: __('Operate'), events: {
                        		'click .btn-dispose': function(e, value, row, index) {
                                	e.stopPropagation();
                                    var id = row.id;
                                    
                                }
	                        }, formatter: function (value, row, index) {
	                            return '<a href="javascript:;" class="btn btn-info btn-dispose btn-xs"><i class="fa fa-cog"></i></a>';
	                        }
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        edit: function () {
            Form.api.bindevent($("form[role=form]"));
            var id = Util.api.query('id');
        	if(id) {
	        	Form.api.fill($('form'), Controller.config.get_url, id, function(data) {
	        	});
        	}
        }
    };
    return Controller;
});