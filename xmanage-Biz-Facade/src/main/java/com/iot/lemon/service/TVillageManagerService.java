package com.iot.lemon.service;

import com.iot.lemon.model.TVillageManager;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 小区管理员  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
public interface TVillageManagerService extends BaseService<TVillageManager> {

}