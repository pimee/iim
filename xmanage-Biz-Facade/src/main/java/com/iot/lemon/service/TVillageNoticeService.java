package com.iot.lemon.service;

import com.iot.lemon.model.TVillageNotice;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 小区公告  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-17
 */
public interface TVillageNoticeService extends BaseService<TVillageNotice> {
	
}