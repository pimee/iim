package com.iot.lemon.service;

import com.iot.lemon.model.TArea;
import com.iot.lemon.model.TBuilding;
import com.iot.lemon.model.THouse;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 房屋  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface THouseService extends BaseService<THouse> {
    void updateManageFee(TArea area);

    void updateManageFee(TBuilding building);
}