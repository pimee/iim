package com.iot.lemon.service;

import java.util.List;
import java.util.Map;

import com.iot.lemon.model.TAttendance;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 巡逻  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TAttendanceService extends BaseService<TAttendance> {
    List<TAttendance> queryPatrol(Map<String, Object> param);
}