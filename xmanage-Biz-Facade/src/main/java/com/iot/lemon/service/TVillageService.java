package com.iot.lemon.service;

import com.iot.lemon.model.TVillage;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 小区  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
public interface TVillageService extends BaseService<TVillage> {
	
}