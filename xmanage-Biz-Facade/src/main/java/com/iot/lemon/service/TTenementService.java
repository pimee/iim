package com.iot.lemon.service;

import com.iot.lemon.model.TTenement;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 住户  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TTenementService extends BaseService<TTenement> {
	
}