package com.iot.lemon.service;

import com.iot.lemon.model.TRepair;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 保修维修  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TRepairService extends BaseService<TRepair> {
	
}