package com.iot.lemon.service;

import com.iot.lemon.model.TAppVersion;

import top.ibase4j.core.base.BaseService;

/**
 * @author ShenHuaJie
 * @since 2018年6月8日 下午9:13:55
 */
public interface AppVersionService extends BaseService<TAppVersion> {

}
