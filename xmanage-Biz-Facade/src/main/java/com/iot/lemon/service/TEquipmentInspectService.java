package com.iot.lemon.service;

import com.iot.lemon.model.TEquipmentInspect;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 设备巡检记录  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TEquipmentInspectService extends BaseService<TEquipmentInspect> {
	
}