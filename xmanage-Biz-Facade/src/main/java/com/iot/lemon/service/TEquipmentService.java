package com.iot.lemon.service;

import com.iot.lemon.model.TEquipment;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 设备  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TEquipmentService extends BaseService<TEquipment> {
	
}