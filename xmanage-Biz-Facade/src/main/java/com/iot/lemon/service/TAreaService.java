package com.iot.lemon.service;

import com.iot.lemon.model.TArea;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 区域  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TAreaService extends BaseService<TArea> {
	
}