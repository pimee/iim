package com.iot.lemon.service;

import com.iot.lemon.model.TCar;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 车辆  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TCarService extends BaseService<TCar> {
	
}