package com.iot.lemon.service;

import com.iot.lemon.model.TCarPayment;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 车辆缴费记录  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TCarPaymentService extends BaseService<TCarPayment> {
	
}