package com.iot.lemon.service;

import com.iot.lemon.model.TCarInout;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 车辆出入记录  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TCarInoutService extends BaseService<TCarInout> {
	
}