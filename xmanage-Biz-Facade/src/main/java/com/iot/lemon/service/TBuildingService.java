package com.iot.lemon.service;

import com.iot.lemon.model.TBuilding;
import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 楼栋  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
public interface TBuildingService extends BaseService<TBuilding> {
	
}