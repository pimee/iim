package com.iot.lemon.service;

import java.util.Map;

import com.iot.lemon.model.TOrder;

import top.ibase4j.core.base.BaseService;

/**
 * <p>
 * 订单  服务类
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-19
 */
public interface TOrderService extends BaseService<TOrder> {
    /**
     * 业务类型
     * @author ShenHuaJie
     * @since 2018年9月19日 下午2:07:23
     */
    interface BizType {
        /** 物业费 */
        final String SERVICE_FEE = "01";
        /** 维修费 */
        final String REPAIRE_FEE = "02";
        /** 停车费 */
        final String CAR_STOP_FEE = "11";
        /** 停车费充值 */
        final String CAR_RECHARGE = "12";
    }

    /**
     * 支付方式
     * @author ShenHuaJie
     * @since 2018年9月19日 下午2:07:10
     */
    interface PayType {
        /** 微信 */
        final String WECHAT = "1";
        /** 支付宝 */
        final String ALIPAY = "2";
    }

    TOrder updateOrder(TOrder record);

    void updateWxPayReturn(Map<String, Object> param);

    void updateAliPayReturn(Map<String, Object> param);
}