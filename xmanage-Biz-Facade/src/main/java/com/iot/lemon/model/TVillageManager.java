package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 小区管理员
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@ApiModel("小区管理员")
@TableName("t_village_manager")
@SuppressWarnings("serial")
public class TVillageManager extends BaseModel {

    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "用户编号")
    @TableField("user_id")
    private Long userId;

    @TableField(exist = false)
    private String userName;

    public Long getVillageId() {
        return villageId;
    }

    public void setVillageId(Long villageId) {
        this.villageId = villageId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}