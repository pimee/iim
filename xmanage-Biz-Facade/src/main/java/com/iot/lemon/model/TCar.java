package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 车辆
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("车辆")
@TableName("t_car")
@SuppressWarnings("serial")
public class TCar extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "车牌号")
    @TableField("plate_number")
    private String plateNumber;
    @ApiModelProperty(value = "车辆型号")
    @TableField("car_type")
    private String carType;
    @ApiModelProperty(value = "房屋编号")
    @TableField("house_id")
    private Long houseId;
    @ApiModelProperty(value = "联系人")
    @TableField("link_man")
    private String linkMan;
    @ApiModelProperty(value = "联系人电话")
    @TableField("link_phone")
    private String linkPhone;
    @ApiModelProperty(value = "车位类型")
    @TableField("carport_type")
    private String carportType;
    @ApiModelProperty(value = "车位编号")
    @TableField("carport_no")
    private String carportNo;
    @ApiModelProperty(value = "车辆状态")
    @TableField("car_status")
    private String carStatus;

    @TableField(exist = false)
    private String houseName;

    public Long getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TCar setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public TCar setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
        return this;
    }

    public String getCarType() {
        return carType;
    }

    public TCar setCarType(String carType) {
        this.carType = carType;
        return this;
    }

    public Long getHouseId() {
        return houseId;
    }

    public TCar setHouseId(Long houseId) {
        this.houseId = houseId;
        return this;
    }

    public String getLinkMan() {
        return linkMan;
    }

    public TCar setLinkMan(String linkMan) {
        this.linkMan = linkMan;
        return this;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public TCar setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
        return this;
    }

    public String getCarportType() {
        return carportType;
    }

    public TCar setCarportType(String carportType) {
        this.carportType = carportType;
        return this;
    }

    public String getCarportNo() {
        return carportNo;
    }

    public TCar setCarportNo(String carportNo) {
        this.carportNo = carportNo;
        return this;
    }

    public String getCarStatus() {
        return carStatus;
    }

    public void setCarStatus(String carStatus) {
        this.carStatus = carStatus;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }
}