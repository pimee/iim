package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 房屋
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("房屋")
@TableName("t_house")
@SuppressWarnings("serial")
public class THouse extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "楼栋编号")
    @TableField("building_id")
    private Long buildingId;
    @ApiModelProperty(value = "房屋编号")
    @TableField("house_no")
    private String houseNo;
    @ApiModelProperty(value = "产权面积")
    @TableField("own_area")
    private BigDecimal ownArea;
    @ApiModelProperty(value = "房屋状态(1空置2装修中3自住4出租)")
    @TableField("house_status")
    private String houseStatus;
    @ApiModelProperty(value = "产权状态(0未办理2已办理)")
    @TableField("own_status")
    private String ownStatus;
    @ApiModelProperty(value = "卧室数")
    private Integer bedroom;
    @ApiModelProperty(value = "客厅数")
    private Integer livingroom;
    @ApiModelProperty(value = "卫生间数")
    @TableField("toilet_")
    private Integer toilet;
    @ApiModelProperty(value = "是否集中供暖")
    @TableField("central_heating")
    private String centralHeating;
    @ApiModelProperty(value = "物业经理")
    @TableField("manager_id")
    private Long managerId;
    @TableField("manage_fee")
    private BigDecimal manageFee;

    @TableField(exist = false)
    private Long areaId;
    @TableField(exist = false)
    private String areaName;
    @TableField(exist = false)
    private String buildingName;
    @TableField(exist = false)
    private String houseType;
    @TableField(exist = false)
    private String ownerName;
    @TableField(exist = false)
    private String ownerPhone;
    @TableField(exist = false)
    private String ownerCard;
    @TableField(exist = false)
    private List<Long> orgaIds;

    public Long getOrgaId() {
        return orgaId;
    }

    public THouse setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public Long getVillageId() {
        return villageId;
    }

    public THouse setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public THouse setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public THouse setHouseNo(String houseNo) {
        this.houseNo = houseNo;
        return this;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public THouse setOwnerName(String ownerName) {
        this.ownerName = ownerName;
        return this;
    }

    public String getOwnerCard() {
        return ownerCard;
    }

    public THouse setOwnerCard(String ownerCard) {
        this.ownerCard = ownerCard;
        return this;
    }

    public BigDecimal getOwnArea() {
        return ownArea;
    }

    public THouse setOwnArea(BigDecimal ownArea) {
        this.ownArea = ownArea;
        return this;
    }

    public String getHouseStatus() {
        return houseStatus;
    }

    public THouse setHouseStatus(String houseStatus) {
        this.houseStatus = houseStatus;
        return this;
    }

    public String getOwnStatus() {
        return ownStatus;
    }

    public THouse setOwnStatus(String ownStatus) {
        this.ownStatus = ownStatus;
        return this;
    }

    public Integer getBedroom() {
        return bedroom;
    }

    public THouse setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
        return this;
    }

    public Integer getLivingroom() {
        return livingroom;
    }

    public THouse setLivingroom(Integer livingroom) {
        this.livingroom = livingroom;
        return this;
    }

    public Integer getToilet() {
        return toilet;
    }

    public THouse setToilet(Integer toilet) {
        this.toilet = toilet;
        return this;
    }

    public String getCentralHeating() {
        return centralHeating;
    }

    public THouse setCentralHeating(String centralHeating) {
        this.centralHeating = centralHeating;
        return this;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getAreaName() {
        return areaName;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getHouseType() {
        return houseType;
    }

    public void setHouseType(String houseType) {
        this.houseType = houseType;
    }

    public String getOwnerPhone() {
        return ownerPhone;
    }

    public void setOwnerPhone(String ownerPhone) {
        this.ownerPhone = ownerPhone;
    }

    public List<Long> getOrgaIds() {
        return orgaIds;
    }

    public void setOrgaIds(List<Long> orgaIds) {
        this.orgaIds = orgaIds;
    }

    public BigDecimal getManageFee() {
        return manageFee;
    }

    public THouse setManageFee(BigDecimal manageFee) {
        this.manageFee = manageFee;
        return this;
    }
}