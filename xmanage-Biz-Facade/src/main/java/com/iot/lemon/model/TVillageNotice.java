package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 小区公告
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-17
 */
@ApiModel("小区公告")
@TableName("t_village_notice")
@SuppressWarnings("serial")
public class TVillageNotice extends BaseModel {

    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "公告标题")
    @TableField("notice_title")
    private String noticeTitle;
    @ApiModelProperty(value = "公告类型")
    @TableField("notice_type")
    private String noticeType;
    @ApiModelProperty(value = "内容")
    @TableField("content_")
    private String content;

    public Long getVillageId() {
        return villageId;
    }

    public TVillageNotice setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public TVillageNotice setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
        return this;
    }

    public String getNoticeType() {
        return noticeType;
    }

    public TVillageNotice setNoticeType(String noticeType) {
        this.noticeType = noticeType;
        return this;
    }

    public String getContent() {
        return content;
    }

    public TVillageNotice setContent(String content) {
        this.content = content;
        return this;
    }

}