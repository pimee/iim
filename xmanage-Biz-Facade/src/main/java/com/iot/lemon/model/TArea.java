package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 区域
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("区域")
@TableName("t_area")
@SuppressWarnings("serial")
public class TArea extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @TableField("area_name")
    private String areaName;
    @TableField("manage_fee")
    private BigDecimal manageFee;

    @TableField(exist = false)
    private List<Long> orgaIds;

    public Long getOrgaId() {
        return orgaId;
    }

    public TArea setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TArea setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public String getAreaName() {
        return areaName;
    }

    public TArea setAreaName(String areaName) {
        this.areaName = areaName;
        return this;
    }

    public List<Long> getOrgaIds() {
        return orgaIds;
    }

    public void setOrgaIds(List<Long> orgaIds) {
        this.orgaIds = orgaIds;
    }

    public BigDecimal getManageFee() {
        return manageFee;
    }

    public void setManageFee(BigDecimal manageFee) {
        this.manageFee = manageFee;
    }
}