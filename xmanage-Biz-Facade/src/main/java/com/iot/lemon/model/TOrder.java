package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 订单
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-19
 */
@ApiModel("订单")
@TableName("t_order")
@SuppressWarnings("serial")
public class TOrder extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "房屋编号")
    @TableField("house_id")
    private Long houseId;
    @ApiModelProperty(value = "账单年月")
    @TableField("order_date")
    private String orderDate;
    @ApiModelProperty(value = "业务类型：1停车费，2停车费充值")
    @TableField("biz_type")
    private String bizType;
    @ApiModelProperty(value = "订单号")
    @TableField("order_no")
    private String orderNo;
    @ApiModelProperty(value = "下单时间")
    @TableField("order_time")
    private Date orderTime;
    @ApiModelProperty(value = "金额")
    @TableField("order_money")
    private BigDecimal orderMoney;
    @ApiModelProperty(value = "支付方式：1微信，2支付宝")
    @TableField("pay_type")
    private String payType;
    @TableField("pay_qrcode")
    private String payQrcode;
    @ApiModelProperty(value = "支付流水号")
    @TableField("pay_order_id")
    private String payOrderId;
    @ApiModelProperty(value = "支付结果")
    @TableField("pay_result")
    private String payResult;
    @ApiModelProperty(value = "支付时间")
    @TableField("pay_time")
    private Date payTime;
    @ApiModelProperty(value = "订单状态")
    @TableField("state_")
    private String state;

    @TableField(exist = false)
    private String clientIP;
    @TableField(exist = false)
    private String houseNo;

    public Long getVillageId() {
        return villageId;
    }

    public TOrder setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public String getBizType() {
        return bizType;
    }

    public TOrder setBizType(String bizType) {
        this.bizType = bizType;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public TOrder setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public TOrder setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
        return this;
    }

    public BigDecimal getOrderMoney() {
        return orderMoney;
    }

    public TOrder setOrderMoney(BigDecimal orderMoney) {
        this.orderMoney = orderMoney;
        return this;
    }

    public String getPayType() {
        return payType;
    }

    public TOrder setPayType(String payType) {
        this.payType = payType;
        return this;
    }

    public String getPayQrcode() {
        return payQrcode;
    }

    public TOrder setPayQrcode(String payQrcode) {
        this.payQrcode = payQrcode;
        return this;
    }

    public String getPayOrderId() {
        return payOrderId;
    }

    public TOrder setPayOrderId(String payOrderId) {
        this.payOrderId = payOrderId;
        return this;
    }

    public String getPayResult() {
        return payResult;
    }

    public TOrder setPayResult(String payResult) {
        this.payResult = payResult;
        return this;
    }

    public Date getPayTime() {
        return payTime;
    }

    public TOrder setPayTime(Date payTime) {
        this.payTime = payTime;
        return this;
    }

    public String getState() {
        return state;
    }

    public TOrder setState(String state) {
        this.state = state;
        return this;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public Long getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
    }

    public Long getHouseId() {
        return houseId;
    }

    public TOrder setHouseId(Long houseId) {
        this.houseId = houseId;
        return this;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public TOrder setOrderDate(String orderDate) {
        this.orderDate = orderDate;
        return this;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }
}