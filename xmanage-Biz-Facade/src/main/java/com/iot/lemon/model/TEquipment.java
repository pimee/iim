package com.iot.lemon.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 设备
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("设备")
@TableName("t_equipment")
@SuppressWarnings("serial")
public class TEquipment extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "楼栋编号")
    @TableField("building_id")
    private Long buildingId;
    @ApiModelProperty(value = "设备名称")
    @TableField("name_")
    private String name;
    @ApiModelProperty(value = "设备编号")
    @TableField("no_")
    private String no;
    @ApiModelProperty(value = "设备类型")
    @TableField("type_")
    private String type;
    @ApiModelProperty(value = "设备状态")
    @TableField("status_")
    private String status;
    @ApiModelProperty(value = "巡检人员")
    @TableField("inspector_id")
    private Long inspectorId;
    @ApiModelProperty(value = "最后巡检时间")
    @TableField("last_inspect_time")
    private Date lastInspectTime;
    @ApiModelProperty(value = "存放位置")
    @TableField("location_")
    private String location;

    @TableField(exist = false)
    private Long areaId;
    @TableField(exist = false)
    private String inspectorName;
    @TableField(exist = false)
    private String inspectorPhone;
    @TableField(exist = false)
    private String areaName;
    @TableField(exist = false)
    private String buildingName;

    public Long getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TEquipment setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public TEquipment setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
        return this;
    }

    public String getName() {
        return name;
    }

    public TEquipment setName(String name) {
        this.name = name;
        return this;
    }

    public String getNo() {
        return no;
    }

    public TEquipment setNo(String no) {
        this.no = no;
        return this;
    }

    public String getType() {
        return type;
    }

    public TEquipment setType(String type) {
        this.type = type;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public TEquipment setStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getInspectorId() {
        return inspectorId;
    }

    public TEquipment setInspectorId(Long inspectorId) {
        this.inspectorId = inspectorId;
        return this;
    }

    public String getInspectorName() {
        return inspectorName;
    }

    public TEquipment setInspectorName(String inspectorName) {
        this.inspectorName = inspectorName;
        return this;
    }

    public String getInspectorPhone() {
        return inspectorPhone;
    }

    public TEquipment setInspectorPhone(String inspectorPhone) {
        this.inspectorPhone = inspectorPhone;
        return this;
    }

    public Date getLastInspectTime() {
        return lastInspectTime;
    }

    public TEquipment setLastInspectTime(Date lastInspectTime) {
        this.lastInspectTime = lastInspectTime;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public TEquipment setLocation(String location) {
        this.location = location;
        return this;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }
}