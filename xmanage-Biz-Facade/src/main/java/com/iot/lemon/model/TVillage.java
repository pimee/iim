package com.iot.lemon.model;

import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 小区
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-07
 */
@ApiModel("小区")
@TableName("t_village")
@SuppressWarnings("serial")
public class TVillage extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区名称")
    @TableField("village_name")
    private String villageName;
    @ApiModelProperty(value = "所在省市")
    @TableField("location_province")
    private String locationProvince;
    @ApiModelProperty(value = "所在市")
    @TableField("location_city")
    private String locationCity;
    @ApiModelProperty(value = "所在区")
    @TableField("location_area")
    private String locationArea;
    @ApiModelProperty(value = "详细地址")
    @TableField("location_detail")
    private String locationDetail;
    @ApiModelProperty(value = "建成年份")
    @TableField("build_time")
    private Integer buildTime;
    @TableField("ali_appid")
    private String aliAppid;
    @TableField("ali_private_key")
    private String aliPrivateKey;
    @TableField("ali_public_key")
    private String aliPublicKey;
    @TableField("wx_mch_id")
    private String wxMchId;
    @TableField("wx_appid")
    private String wxAppid;
    @TableField("wx_sign_key")
    private String wxSignKey;

    @TableField(exist = false)
    private List<TVillageManager> managers;
    @TableField(exist = false)
    private String managerNames;
    @TableField(exist = false)
    private Long managerId;
    @TableField(exist = false)
    private String location;
    @TableField(exist = false)
    private String orgaName;

    public Long getOrgaId() {
        return orgaId;
    }

    public TVillage setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public String getVillageName() {
        return villageName;
    }

    public TVillage setVillageName(String villageName) {
        this.villageName = villageName;
        return this;
    }

    public String getLocationProvince() {
        return locationProvince;
    }

    public void setLocationProvince(String locationProvince) {
        this.locationProvince = locationProvince;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationArea() {
        return locationArea;
    }

    public void setLocationArea(String locationArea) {
        this.locationArea = locationArea;
    }

    public String getLocationDetail() {
        return locationDetail;
    }

    public void setLocationDetail(String locationDetail) {
        this.locationDetail = locationDetail;
    }

    public Integer getBuildTime() {
        return buildTime;
    }

    public void setBuildTime(Integer buildTime) {
        this.buildTime = buildTime;
    }

    public List<TVillageManager> getManagers() {
        return managers;
    }

    public void setManagers(List<TVillageManager> managers) {
        this.managers = managers;
    }

    public String getManagerNames() {
        return managerNames;
    }

    public void setManagerNames(String managerNames) {
        this.managerNames = managerNames;
    }

    public Long getManagerId() {
        return managerId;
    }

    public TVillage setManagerId(Long managerId) {
        this.managerId = managerId;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrgaName() {
        return orgaName;
    }

    public void setOrgaName(String orgaName) {
        this.orgaName = orgaName;
    }

    public String getAliAppid() {
        return aliAppid;
    }

    public void setAliAppid(String aliAppid) {
        this.aliAppid = aliAppid;
    }

    public String getAliPrivateKey() {
        return aliPrivateKey;
    }

    public void setAliPrivateKey(String aliPrivateKey) {
        this.aliPrivateKey = aliPrivateKey;
    }

    public String getAliPublicKey() {
        return aliPublicKey;
    }

    public void setAliPublicKey(String aliPublicKey) {
        this.aliPublicKey = aliPublicKey;
    }

    public String getWxMchId() {
        return wxMchId;
    }

    public void setWxMchId(String wxMchId) {
        this.wxMchId = wxMchId;
    }

    public String getWxAppid() {
        return wxAppid;
    }

    public void setWxAppid(String wxAppid) {
        this.wxAppid = wxAppid;
    }

    public String getWxSignKey() {
        return wxSignKey;
    }

    public void setWxSignKey(String wxSignKey) {
        this.wxSignKey = wxSignKey;
    }
}
