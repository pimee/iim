package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 住户
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("住户")
@TableName("t_tenement")
@SuppressWarnings("serial")
public class TTenement extends BaseModel {
    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "房屋编号")
    @TableField("house_id")
    private Long houseId;
    @ApiModelProperty(value = "姓名")
    @TableField("full_name")
    private String fullName;
    @ApiModelProperty(value = "联系方式")
    @TableField("phone_")
    private String phone;
    @ApiModelProperty(value = "类型")
    @TableField("ment_type")
    private String mentType;
    @ApiModelProperty(value = "身份证号")
    @TableField("idcard_")
    private String idcard;
    @ApiModelProperty(value = "密码")
    @TableField("password_")
    private String password;
    @ApiModelProperty(value = "头像")
    @TableField("avatar_")
    private String avatar;
    @ApiModelProperty(value = "性别(0:未知;1:男;2:女)")
    @TableField("sex_")
    private Integer sex;
    @ApiModelProperty(value = "APP会话token")
    @TableField("token_")
    private String token;
    @ApiModelProperty(value = "是否在线")
    @TableField("is_online")
    private Integer isOnline;

    @TableField(exist = false)
    private Integer carQuantity;
    @TableField(exist = false)
    private Long areaId;
    @TableField(exist = false)
    private Long buildingId;
    @TableField(exist = false)
    private String houseName;

    public Long getOrgaId() {
        return orgaId;
    }

    public TTenement setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TTenement setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public Long getHouseId() {
        return houseId;
    }

    public TTenement setHouseId(Long houseId) {
        this.houseId = houseId;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public TTenement setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public TTenement setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getMentType() {
        return mentType;
    }

    public TTenement setMentType(String mentType) {
        this.mentType = mentType;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Integer isOnline) {
        this.isOnline = isOnline;
    }

    public Integer getCarQuantity() {
        return carQuantity;
    }

    public TTenement setCarQuantity(Integer carQuantity) {
        this.carQuantity = carQuantity;
        return this;
    }

    public String getHouseName() {
        return houseName;
    }

    public void setHouseName(String houseName) {
        this.houseName = houseName;
    }

    public String getIdcard() {
        return idcard;
    }

    public TTenement setIdcard(String idcard) {
        this.idcard = idcard;
        return this;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Long getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(Long buildingId) {
        this.buildingId = buildingId;
    }
}