package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 车辆出入记录
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("车辆出入记录")
@TableName("t_car_inout")
@SuppressWarnings("serial")
public class TCarInout extends BaseModel {

    @ApiModelProperty(value = "车辆编号")
    @TableField("car_id")
    private Long carId;
    @ApiModelProperty(value = "类型")
    @TableField("in_out")
    private String inOut;
    @ApiModelProperty(value = "金额")
    @TableField("money_")
    private BigDecimal money;
    @ApiModelProperty(value = "出入口")
    @TableField("in_out_door")
    private String inOutDoor;
    @ApiModelProperty(value = "出入时间")
    @TableField("in_out_time")
    private Date inOutTime;
    @ApiModelProperty(value = "订单号")
    @TableField("order_no")
    private String orderNo;
    @ApiModelProperty(value = "缴费方式")
    @TableField("pay_way")
    private String payWay;
    @ApiModelProperty(value = "停车模式")
    @TableField("park_model")
    private String parkModel;


    public Long getCarId() {
        return carId;
    }

    public TCarInout setCarId(Long carId) {
        this.carId = carId;
        return this;
    }

    public String getInOut() {
        return inOut;
    }

    public TCarInout setInOut(String inOut) {
        this.inOut = inOut;
        return this;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public TCarInout setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public String getInOutDoor() {
        return inOutDoor;
    }

    public TCarInout setInOutDoor(String inOutDoor) {
        this.inOutDoor = inOutDoor;
        return this;
    }

    public Date getInOutTime() {
        return inOutTime;
    }

    public TCarInout setInOutTime(Date inOutTime) {
        this.inOutTime = inOutTime;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public TCarInout setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

    public String getPayWay() {
        return payWay;
    }

    public TCarInout setPayWay(String payWay) {
        this.payWay = payWay;
        return this;
    }

    public String getParkModel() {
        return parkModel;
    }

    public TCarInout setParkModel(String parkModel) {
        this.parkModel = parkModel;
        return this;
    }

}