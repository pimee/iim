package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.List;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 楼栋
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("楼栋")
@TableName("t_building")
@SuppressWarnings("serial")
public class TBuilding extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "区域编号")
    @TableField("area_id")
    private Long areaId;
    @ApiModelProperty(value = "楼栋名称")
    @TableField("building_name")
    private String buildingName;
    @ApiModelProperty(value = "物业经理")
    @TableField("manager_id")
    private Long managerId;
    @ApiModelProperty(value = "房屋类型(1住宅2别墅3写字楼4商铺5自住型商品房)")
    @TableField("house_type")
    private String houseType;
    @ApiModelProperty(value = "电梯数")
    @TableField("elevator_num")
    private Integer elevatorNum;
    @ApiModelProperty(value = "总楼层")
    @TableField("total_floor")
    private Integer totalFloor;
    @ApiModelProperty(value = "单元数")
    @TableField("total_unit")
    private Integer totalUnit;
    @TableField("manage_fee")
    private BigDecimal manageFee;

    @TableField(exist = false)
    private Integer totalHouse;
    @TableField(exist = false)
    private String areaName;
    @TableField(exist = false)
    private String managerName;
    @TableField(exist = false)
    private String managerPhone;
    @TableField(exist = false)
    private Integer equipmentNum;
    @TableField(exist = false)
    private List<String> houseNos;
    @TableField(exist = false)
    private List<Long> orgaIds;

    public Long getOrgaId() {
        return orgaId;
    }

    public TBuilding setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TBuilding setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public Long getAreaId() {
        return areaId;
    }

    public TBuilding setAreaId(Long areaId) {
        this.areaId = areaId;
        return this;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public TBuilding setBuildingName(String buildingName) {
        this.buildingName = buildingName;
        return this;
    }

    public String getHouseType() {
        return houseType;
    }

    public TBuilding setHouseType(String houseType) {
        this.houseType = houseType;
        return this;
    }

    public Integer getElevatorNum() {
        return elevatorNum;
    }

    public TBuilding setElevatorNum(Integer elevatorNum) {
        this.elevatorNum = elevatorNum;
        return this;
    }

    public Integer getTotalFloor() {
        return totalFloor;
    }

    public TBuilding setTotalFloor(Integer totalFloor) {
        this.totalFloor = totalFloor;
        return this;
    }

    public Integer getTotalUnit() {
        return totalUnit;
    }

    public TBuilding setTotalUnit(Integer totalUnit) {
        this.totalUnit = totalUnit;
        return this;
    }

    public Long getManagerId() {
        return managerId;
    }

    public void setManagerId(Long managerId) {
        this.managerId = managerId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerPhone() {
        return managerPhone;
    }

    public void setManagerPhone(String managerPhone) {
        this.managerPhone = managerPhone;
    }

    public Integer getEquipmentNum() {
        return equipmentNum;
    }

    public void setEquipmentNum(Integer equipmentNum) {
        this.equipmentNum = equipmentNum;
    }

    public List<String> getHouseNos() {
        return houseNos;
    }

    public void setHouseNos(List<String> houseNos) {
        this.houseNos = houseNos;
    }

    public List<Long> getOrgaIds() {
        return orgaIds;
    }

    public void setOrgaIds(List<Long> orgaIds) {
        this.orgaIds = orgaIds;
    }

    public BigDecimal getManageFee() {
        return manageFee;
    }

    public void setManageFee(BigDecimal manageFee) {
        this.manageFee = manageFee;
    }

    public Integer getTotalHouse() {
        return totalHouse;
    }

    public void setTotalHouse(Integer totalHouse) {
        this.totalHouse = totalHouse;
    }
}