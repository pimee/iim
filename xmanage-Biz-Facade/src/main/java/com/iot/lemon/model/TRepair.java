package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 保修维修
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("保修维修")
@TableName("t_repair")
@SuppressWarnings("serial")
public class TRepair extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "工单号")
    @TableField("work_no")
    private String workNo;
    @ApiModelProperty(value = "房屋编号")
    @TableField("house_id")
    private Long houseId;
    @ApiModelProperty(value = "类型")
    @TableField("work_type")
    private String workType;
    @ApiModelProperty(value = "费用")
    @TableField("work_fee")
    private BigDecimal workFee;
    @ApiModelProperty(value = "详情")
    @TableField("work_detail")
    private String workDetail;
    @ApiModelProperty(value = "处置人")
    @TableField("disposer_id")
    private Long disposerId;
    @ApiModelProperty(value = "接受时间")
    @TableField("accept_time")
    private Date acceptTime;

    public Long getOrgaId() {
        return orgaId;
    }

    public void setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
    }

    public Long getVillageId() {
        return villageId;
    }

    public void setVillageId(Long villageId) {
        this.villageId = villageId;
    }

    public String getWorkNo() {
        return workNo;
    }

    public TRepair setWorkNo(String workNo) {
        this.workNo = workNo;
        return this;
    }

    public String getWorkType() {
        return workType;
    }

    public TRepair setWorkType(String workType) {
        this.workType = workType;
        return this;
    }

    public BigDecimal getWorkFee() {
        return workFee;
    }

    public TRepair setWorkFee(BigDecimal workFee) {
        this.workFee = workFee;
        return this;
    }

    public String getWorkDetail() {
        return workDetail;
    }

    public TRepair setWorkDetail(String workDetail) {
        this.workDetail = workDetail;
        return this;
    }

    public Long getDisposerId() {
        return disposerId;
    }

    public TRepair setDisposerId(Long disposerId) {
        this.disposerId = disposerId;
        return this;
    }

    public Date getAcceptTime() {
        return acceptTime;
    }

    public TRepair setAcceptTime(Date acceptTime) {
        this.acceptTime = acceptTime;
        return this;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }
}