package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 车辆缴费记录
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("车辆缴费记录")
@TableName("t_car_payment")
@SuppressWarnings("serial")
public class TCarPayment extends BaseModel {

    @ApiModelProperty(value = "车辆编号")
    @TableField("car_id")
    private Long carId;
    @ApiModelProperty(value = "金额")
    @TableField("money_")
    private BigDecimal money;
    @ApiModelProperty(value = "缴费方式")
    @TableField("pay_type")
    private String payType;
    @ApiModelProperty(value = "缴费时间")
    @TableField("pay_time")
    private Date payTime;
    @ApiModelProperty(value = "订单号")
    @TableField("order_no")
    private String orderNo;

    public Long getCarId() {
        return carId;
    }

    public TCarPayment setCarId(Long carId) {
        this.carId = carId;
        return this;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public TCarPayment setMoney(BigDecimal money) {
        this.money = money;
        return this;
    }

    public String getPayType() {
        return payType;
    }

    public TCarPayment setPayType(String payType) {
        this.payType = payType;
        return this;
    }

    public Date getPayTime() {
        return payTime;
    }

    public TCarPayment setPayTime(Date payTime) {
        this.payTime = payTime;
        return this;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public TCarPayment setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        return this;
    }

}