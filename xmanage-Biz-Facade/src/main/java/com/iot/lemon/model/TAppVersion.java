package com.iot.lemon.model;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 客户端版本管理
 * </p>
 *
 * @author ShenHuaJie
 * @since 2017-11-13
 */
@ApiModel("客户端版本管理")
@TableName("t_app_version")
@SuppressWarnings("serial")
public class TAppVersion extends BaseModel {

    @ApiModelProperty(value = "平台：IOS/ANDROID")
    @TableField("platform_")
    private String platform;
    @ApiModelProperty(value = "版本名")
    @TableField("version_")
    private String version;
    @ApiModelProperty(value = "地址")
    @TableField("url_")
    private String url;
    @ApiModelProperty(value = "app大小")
    @TableField("app_size")
    private String appSize;
    @ApiModelProperty(value = "更新内容")
    @TableField("content_")
    private String content;
    @ApiModelProperty(value = "版本号")
    @TableField("version_code")
    private Integer versionCode;

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAppSize() {
        return appSize;
    }

    public void setAppSize(String appSize) {
        this.appSize = appSize;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}