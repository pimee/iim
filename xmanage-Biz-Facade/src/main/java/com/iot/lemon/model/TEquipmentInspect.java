package com.iot.lemon.model;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import top.ibase4j.core.base.BaseModel;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 设备巡检记录
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-08-18
 */
@ApiModel("设备巡检记录")
@TableName("t_equipment_inspect")
@SuppressWarnings("serial")
public class TEquipmentInspect extends BaseModel {

    @ApiModelProperty(value = "设备编号")
	@TableField("equipment_id")
	private Long equipmentId;
    @ApiModelProperty(value = "巡检人")
	@TableField("inspector_")
	private String inspector;
    @ApiModelProperty(value = "巡检时间")
	@TableField("inspect_time")
	private Date inspectTime;
    @ApiModelProperty(value = "设备状态")
	@TableField("equipment_status")
	private String equipmentStatus;
    @ApiModelProperty(value = "工单")
	@TableField("work_order")
	private String workOrder;
    @ApiModelProperty(value = "工单状态")
	@TableField("order_status")
	private String orderStatus;


	public Long getEquipmentId() {
		return equipmentId;
	}

	public TEquipmentInspect setEquipmentId(Long equipmentId) {
		this.equipmentId = equipmentId;
		return this;
	}

	public String getInspector() {
		return inspector;
	}

	public TEquipmentInspect setInspector(String inspector) {
		this.inspector = inspector;
		return this;
	}

	public Date getInspectTime() {
		return inspectTime;
	}

	public TEquipmentInspect setInspectTime(Date inspectTime) {
		this.inspectTime = inspectTime;
		return this;
	}

	public String getEquipmentStatus() {
		return equipmentStatus;
	}

	public TEquipmentInspect setEquipmentStatus(String equipmentStatus) {
		this.equipmentStatus = equipmentStatus;
		return this;
	}

	public String getWorkOrder() {
		return workOrder;
	}

	public TEquipmentInspect setWorkOrder(String workOrder) {
		this.workOrder = workOrder;
		return this;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public TEquipmentInspect setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
		return this;
	}

}