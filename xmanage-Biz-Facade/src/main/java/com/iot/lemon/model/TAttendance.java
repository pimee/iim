package com.iot.lemon.model;

import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import top.ibase4j.core.base.BaseModel;

/**
 * <p>
 * 巡逻
 * </p>
 *
 * @author ShenHuaJie
 * @since 2018-09-03
 */
@ApiModel("巡逻")
@TableName("t_attendance")
@SuppressWarnings("serial")
public class TAttendance extends BaseModel {

    @ApiModelProperty(value = "组织编号")
    @TableField("orga_id")
    private Long orgaId;
    @ApiModelProperty(value = "小区编号")
    @TableField("village_id")
    private Long villageId;
    @ApiModelProperty(value = "所属区域")
    @TableField("area_id")
    private Long areaId;
    @ApiModelProperty(value = "员工编号")
    @TableField("staff_id")
    private Long staffId;
    @ApiModelProperty(value = "上班时间")
    @TableField("sign_time1")
    private Date signTime1;
    @ApiModelProperty(value = "下班时间")
    @TableField("sign_time2")
    private Date signTime2;

    @TableField(exist = false)
    private String staffName;
    @TableField(exist = false)
    private String villageName;
    @TableField(exist = false)
    private String areaName;
    @TableField(exist = false)
    private BigDecimal lon;
    @TableField(exist = false)
    private BigDecimal lat;

    public Long getOrgaId() {
        return orgaId;
    }

    public TAttendance setOrgaId(Long orgaId) {
        this.orgaId = orgaId;
        return this;
    }

    public Long getVillageId() {
        return villageId;
    }

    public TAttendance setVillageId(Long villageId) {
        this.villageId = villageId;
        return this;
    }

    public Long getAreaId() {
        return areaId;
    }

    public TAttendance setAreaId(Long areaId) {
        this.areaId = areaId;
        return this;
    }

    public Long getStaffId() {
        return staffId;
    }

    public TAttendance setStaffId(Long staffId) {
        this.staffId = staffId;
        return this;
    }

    public Date getSignTime1() {
        return signTime1;
    }

    public TAttendance setSignTime1(Date signTime1) {
        this.signTime1 = signTime1;
        return this;
    }

    public Date getSignTime2() {
        return signTime2;
    }

    public TAttendance setSignTime2(Date signTime2) {
        this.signTime2 = signTime2;
        return this;
    }

    public String getStaffName() {
        return staffName;
    }

    public TAttendance setStaffName(String staffName) {
        this.staffName = staffName;
        return this;
    }

    public String getAreaName() {
        return areaName;
    }

    public TAttendance setAreaName(String areaName) {
        this.areaName = areaName;
        return this;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public TAttendance setLon(BigDecimal lon) {
        this.lon = lon;
        return this;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public TAttendance setLat(BigDecimal lat) {
        this.lat = lat;
        return this;
    }

    public String getVillageName() {
        return villageName;
    }

    public TAttendance setVillageName(String villageName) {
        this.villageName = villageName;
        return this;
    }
}